package stjepan.final_project;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import stjepan.final_project.Artists.ArtistsFragment;
import stjepan.final_project.Gigs.Gig;
import stjepan.final_project.Gigs.GigsFragment;
import stjepan.final_project.Lyrics.LyricsFragment;
import stjepan.final_project.Repertoires.RepertoiresFragment;

/**
 * Created by Stjepan on 1.3.2018..
 */

public class Utils {
    public static String capitalize(String s) {
        String[] arr = s.toLowerCase().split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public static String capitalizeFirstChar(String s) {
        s.toLowerCase();
        return s.length() == 0 ? s : s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static void goToFragment(Context ctx, String fragmentName) {
        int fragmentPosition = 0;
        Intent intent = new Intent(ctx, MainActivity.class);
        switch (fragmentName) {
            case LyricsFragment.TITLE:
                fragmentPosition = 0;
                break;
            case ArtistsFragment.TITLE:
                fragmentPosition = 1;
                break;
            case RepertoiresFragment.TITLE:
                fragmentPosition = 2;
                break;
            case GigsFragment.TITLE:
                fragmentPosition = 3;
                break;
        }
        intent.putExtra(MainActivity.KEY_FRAGMENT_NAME, String.valueOf(fragmentPosition));
        ctx.startActivity(intent);
    }
}

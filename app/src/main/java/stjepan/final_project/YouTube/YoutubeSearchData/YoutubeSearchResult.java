package stjepan.final_project.YouTube.YoutubeSearchData;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class YoutubeSearchResult {
    @SerializedName("items") List<YouTubeVideo> videos;

    public YoutubeSearchResult(List<YouTubeVideo> videos) {
        this.videos = videos;
    }

    public List<YouTubeVideo> getVideos() {
        return videos;
    }

    public void setVideos(List<YouTubeVideo> videos) {
        this.videos = videos;
    }
}

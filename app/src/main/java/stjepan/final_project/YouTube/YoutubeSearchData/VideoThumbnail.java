package stjepan.final_project.YouTube.YoutubeSearchData;

import com.google.gson.annotations.SerializedName;

public class VideoThumbnail {
    @SerializedName("url") String url;

    public VideoThumbnail(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

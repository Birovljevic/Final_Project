package stjepan.final_project.YouTube.YoutubeSearchData;

import com.google.gson.annotations.SerializedName;

public class Thumbnails {
    @SerializedName("medium") VideoThumbnail thumbnail;

    public Thumbnails(VideoThumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public VideoThumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(VideoThumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }
}

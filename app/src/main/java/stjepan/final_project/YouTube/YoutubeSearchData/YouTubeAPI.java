package stjepan.final_project.YouTube.YoutubeSearchData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import stjepan.final_project.YouTube.YouTubeConfig;

public interface YouTubeAPI {
    public static final String BASE_URL = "https://www.googleapis.com/youtube/v3/";
    public static final String API_KEY = YouTubeConfig.YOUTUBE_API_KEY;
    public static final String TYPE = "video";

    @GET("search")
    Call<YoutubeSearchResult> searchYouTube(@Query("q") String searchTerm,
                                            @Query("key") String apiKey,
                                            @Query("type") String type,
                                            @Query("part") String part);
}

package stjepan.final_project.YouTube.YoutubeSearchData;

import com.google.gson.annotations.SerializedName;

public class Snippet {
    @SerializedName("title") String title;
    @SerializedName("thumbnails") Thumbnails thumbnail;

    public Snippet(String title, Thumbnails thumbnail) {
        this.title = title;
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Thumbnails getThumbnails() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnails thumbnail) {
        this.thumbnail = thumbnail;
    }
}

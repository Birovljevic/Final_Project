package stjepan.final_project.YouTube.YoutubeSearchData;

import com.google.gson.annotations.SerializedName;

public class YouTubeVideo {
    @SerializedName("snippet") Snippet snippet;
    @SerializedName("id") YoutubeID id;

    public YouTubeVideo(Snippet snippet, YoutubeID id, Thumbnails thumbnail) {
        this.snippet = snippet;
        this.id = id;
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public void setSnippet(Snippet snippet) {
        this.snippet = snippet;
    }

    public YoutubeID getId() {
        return id;
    }

    public void setId(YoutubeID id) {
        this.id = id;
    }
}



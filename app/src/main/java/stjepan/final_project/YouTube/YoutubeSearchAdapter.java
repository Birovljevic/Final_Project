package stjepan.final_project.YouTube;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.final_project.R;
import stjepan.final_project.YouTube.YoutubeSearchData.YouTubeVideo;


public class YoutubeSearchAdapter extends BaseAdapter {

    List<YouTubeVideo> videos;

    public YoutubeSearchAdapter(List<YouTubeVideo> videos) {
        this.videos = videos;
    }

    @Override
    public int getCount() {
        return videos.size();
    }

    @Override
    public Object getItem(int i) {
        return videos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View recycledView, ViewGroup parent) {

        YoutubeVideoHolder holder = null;

        if(recycledView == null){
            recycledView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_youtube_result,parent,false);
            holder = new YoutubeVideoHolder(recycledView);
            recycledView.setTag(holder);
        }else {
            holder = (YoutubeVideoHolder) recycledView.getTag();
        }

        YouTubeVideo video = videos.get(position);

        holder.title.setText(video.getSnippet().getTitle());
        Picasso.with(parent.getContext())
                .load(video.getSnippet().getThumbnails().getThumbnail().getUrl())
                .fit()
                .centerInside()
                .into(holder.thumbnail);

        return recycledView;
    }

    public String getVideoId(int position) {
        return videos.get(position).getId().getVideoID();
    }

    static class YoutubeVideoHolder{

        @BindView(R.id.imageView_itemYoutubeResult_thumbnail) ImageView thumbnail;
        @BindView(R.id.textview_itemYoutubeResult_title) TextView title;

        public YoutubeVideoHolder(View view){
            ButterKnife.bind(this, view);
        }
    }

}

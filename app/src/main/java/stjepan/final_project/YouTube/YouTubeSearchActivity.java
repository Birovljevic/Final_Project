package stjepan.final_project.YouTube;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import stjepan.final_project.R;
import stjepan.final_project.YouTube.YoutubeSearchAdapter;
import stjepan.final_project.YouTube.YoutubeSearchData.YouTubeAPI;
import stjepan.final_project.YouTube.YoutubeSearchData.YoutubeSearchResult;

public class YouTubeSearchActivity extends AppCompatActivity implements Callback<YoutubeSearchResult> {

    public static final String SEARCH_TERM = "SearchTerm";
    public static final String YOUTUBE_LINK = "YouTubeLink";

    Retrofit retrofit;
    YouTubeAPI youTubeAPI;

    private String searchTerm;

    @BindView(R.id.listView_activityYoutubeSearch_resultVideos)
    ListView videos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube_search);
        ButterKnife.bind(this);
        handleStartingIntent(getIntent());
        setUpRetrofit();
        searchYoutubeAndDisplay(searchTerm);
    }

    private void setUpRetrofit() {
        retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(YouTubeAPI.BASE_URL)
                .build();
        youTubeAPI = retrofit.create(YouTubeAPI.class);
    }

    private void handleStartingIntent(Intent intent) {
        if (intent != null && intent.hasExtra(SEARCH_TERM)) {
            searchTerm = intent.getStringExtra(SEARCH_TERM);
        }
    }

    private void searchYoutubeAndDisplay(String searchTerm) {
        Call<YoutubeSearchResult> request
                = youTubeAPI.searchYouTube(searchTerm, YouTubeAPI.API_KEY, YouTubeAPI.TYPE, "snippet");
        request.enqueue(this);
    }

    @OnItemClick(R.id.listView_activityYoutubeSearch_resultVideos)
    public void selectVideo(int position) {
        YoutubeSearchAdapter adapter = (YoutubeSearchAdapter) videos.getAdapter();
        Intent intent = new Intent();
        intent.putExtra(YOUTUBE_LINK, adapter.getVideoId(position));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onResponse(Call<YoutubeSearchResult> call, Response<YoutubeSearchResult> response) {
        YoutubeSearchResult result = response.body();
        YoutubeSearchAdapter adapter = new YoutubeSearchAdapter(result.getVideos());
        videos.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<YoutubeSearchResult> call, Throwable t) {
        Log.e("Error", t.getMessage());
    }
}

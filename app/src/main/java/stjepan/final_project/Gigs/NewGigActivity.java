package stjepan.final_project.Gigs;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import stjepan.final_project.MainActivity;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class NewGigActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private DatabaseReference dbRefGigs;

    @BindView(R.id.etGigTitle)
    EditText etGigTitle;
    @BindView(R.id.etGigAddress)
    EditText etGigAddress;
    @BindView(R.id.etGigNotes)
    EditText etGigNotes;
    @BindView(R.id.tvGigDatePickerShow)
    TextView tvGigDatePickerShow;
    @BindView(R.id.etGigDate)
    EditText etGigDate;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_gig);
        ButterKnife.bind(this);
        this.dbRefGigs = FirebaseDatabase.getInstance().getReference().child(getString(R.string.firebase_gigs));
        this.datePickerDialog = new DatePickerDialog(this, NewGigActivity.this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    @OnClick(R.id.btnSaveGig)
    public void saveGig() {
        String title = this.etGigTitle.getText().toString().trim();
        String address = this.etGigAddress.getText().toString().trim();
        String notes = this.etGigNotes.getText().toString().trim();
        String date = this.etGigDate.getText().toString().trim();
        Gig newGig = new Gig();
        if (address != null && !address.isEmpty() && title != null && !title.isEmpty() && date != null && !date.isEmpty()) {
            newGig.setmTitle(Utils.capitalizeFirstChar(title));
            newGig.setmAddress(address);
            newGig.setmNotes(notes);
            newGig.setmDate(date);
            newGig.setmId(this.dbRefGigs.push().getKey());

            try{
                LatLng geoPoint = getLocationFromAddress(address);
                newGig.setmLatitude(String.valueOf(geoPoint.latitude));
                newGig.setmLongitude(String.valueOf(geoPoint.longitude));
                this.dbRefGigs.child(newGig.getmId()).setValue(newGig);
                Toast.makeText(this, getString(R.string.toast_gig_successfully_saved), Toast.LENGTH_SHORT).show();
                goToMainActivity();
            }catch (Exception e){
                Toast.makeText(this, getString(R.string.toast_error_bad_address), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.toast_error_title_address_are_empty), Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btnCancelGigSaving)
    public void cancelGigSaving() {
        goToMainActivity();
    }

    private void goToMainActivity() {
        Utils.goToFragment(this, GigsFragment.TITLE);
        finish();
    }

    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng geoPoint = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            geoPoint = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return geoPoint;
    }

    @OnClick(R.id.tvGigDatePickerShow)
    public void showDatePicker() {
        this.datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String date = String.valueOf(day) + "." + String.valueOf(month + 1) + "." + String.valueOf(year) + ".";
        this.etGigDate.setText(date);
    }
}

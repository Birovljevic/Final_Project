package stjepan.final_project.Gigs;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import stjepan.final_project.MainActivity;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class GigEditActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    public static final String KEY_GIG_OBJECT_EDIT = "gigObjectEdit";
    private Gig gig;
    private DatabaseReference dbRefGigs;
    private DatePickerDialog datePickerDialog;

    @BindView(R.id.etGigEditTitle)
    EditText etGigEditTitle;
    @BindView(R.id.etGigEditAddress)
    EditText etGigEditAddress;
    @BindView(R.id.etGigEditNotes)
    EditText etGigEditNotes;
    @BindView(R.id.tvGigEditShowDatePicker)
    TextView tvGigEditShowDatePicker;
    @BindView(R.id.etGigEditDate)
    EditText etGigEditDate;
    @BindView(R.id.btnGigEditSave)
    Button btnGigEditSave;
    @BindView(R.id.btnGigEditCancel)
    Button getBtnGigEditCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gig_edit);
        ButterKnife.bind(this);

        this.dbRefGigs = FirebaseDatabase.getInstance().getReference().child(getString(R.string.firebase_gigs));
        Intent startingIntent = getIntent();
        this.handleExtraData(startingIntent);

    }

    private void handleExtraData(Intent startingIntent) {
        if (startingIntent.hasExtra(KEY_GIG_OBJECT_EDIT)) {
            this.gig = (Gig) startingIntent.getSerializableExtra(KEY_GIG_OBJECT_EDIT);
            this.initializeUI();
        } else
            Toast.makeText(this, getString(R.string.toast_error_please_try_again), Toast.LENGTH_LONG).show();
    }

    private void initializeUI() {
        this.etGigEditTitle.setText(gig.getmTitle());
        this.etGigEditAddress.setText(gig.getmAddress());
        this.etGigEditNotes.setText(gig.getmNotes());
        this.etGigEditDate.setText(gig.getmDate());

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        this.datePickerDialog = new DatePickerDialog(
                this, android.R.style.Theme_Holo_Dialog_NoActionBar_MinWidth, GigEditActivity.this,
                year, month, day);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @OnClick(R.id.tvGigEditShowDatePicker)
    public void showDatePicker() {
        this.datePickerDialog.show();
    }

    @OnClick(R.id.btnGigEditCancel)
    public void cancelGigUpdate() {
        Intent intent = new Intent(this, GigDetailsActivity.class);
        intent.putExtra(GigDetailsActivity.KEY_GIG_OBJECT, gig);
        startActivity(intent);
        finish();

    }

    @OnClick(R.id.btnGigEditSave)
    public void updateGig() {
        String title = this.etGigEditTitle.getText().toString().trim();
        String address = this.etGigEditAddress.getText().toString().trim();
        String notes = this.etGigEditNotes.getText().toString().trim();
        String date = this.etGigEditDate.getText().toString().trim();

        if (title != null && !title.isEmpty() && address != null && !address.isEmpty() && date != null && !date.isEmpty()) {
            Gig updatedGig = new Gig();
            updatedGig.setmId(gig.getmId());
            updatedGig.setmTitle(Utils.capitalizeFirstChar(title));
            updatedGig.setmAddress(address);
            updatedGig.setmNotes(notes);
            updatedGig.setmDate(date);

            LatLng geoPoint = getLocationFromAddress(updatedGig.getmAddress());
            updatedGig.setmLatitude(String.valueOf(geoPoint.latitude));
            updatedGig.setmLongitude(String.valueOf(geoPoint.longitude));
            this.dbRefGigs.child(updatedGig.getmId()).setValue(updatedGig);
            goToMain();
        } else
            Toast.makeText(this, getString(R.string.toast_error_title_address_are_empty), Toast.LENGTH_LONG).show();
    }

    private void goToMain() {
        Utils.goToFragment(this, GigsFragment.TITLE);
        finish();
    }

    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng geoPoint = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            geoPoint = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }
        return geoPoint;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String date = String.valueOf(day) + "." + String.valueOf(month + 1) + "." + String.valueOf(year) + ".";
        this.etGigEditDate.setText(date);
    }
}


package stjepan.final_project.Gigs;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import stjepan.final_project.Maps.MapActivity;
import stjepan.final_project.R;

public class GigsFragment extends Fragment implements ValueEventListener {
    public static final String TITLE = "Gigs";
    public static final String KEY_NAME = "gigs";

    private DatabaseReference dbRefGigs;
    private List<Gig> gigsList;
    private GigsAdapter adapter;

    private ActionMode actionModeRef;
    private android.support.v7.widget.Toolbar toolbar;

    @BindView(R.id.lvGigsFragment)
    ListView lvGigsFragment;
    @BindView(R.id.btnAddNewGig)
    Button btnAddNewGig;
    @BindView(R.id.btnShowAllOnMap)
    Button btnShowAllOnMap;


    public GigsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_gigs, container, false);
        ButterKnife.bind(this, view);
        this.dbRefGigs = FirebaseDatabase.getInstance().getReference("gigs");
        this.dbRefGigs.addValueEventListener(this);
        this.setUpListView();
        this.toolbar = getActivity().findViewById(R.id.tMainToolbar);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissCAB();
    }

    private void setUpListView() {
        this.lvGigsFragment.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        this.lvGigsFragment.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
                if (adapter.isSelected(position)) {
                    lvGigsFragment.getChildAt(position).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorGray));
                } else
                    lvGigsFragment.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                actionMode.setTitle(String.valueOf(adapter.getSelectedGigIdsCount()) + " " + getString(R.string.cab_title_selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.menu_cab, menu);
                actionModeRef = actionMode;
                toolbar.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.itemCabSelectAll:
                        adapter.removeSelected();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            lvGigsFragment.getChildAt(i).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorGray));
                            adapter.toggleSelection(i);
                        }
                        actionMode.setTitle(adapter.getCount() + getString(R.string.cab_title_selected));
                        break;
                    case R.id.itemCabDelete:
                        deleteGigConfirmation();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                adapter.removeSelected();
                for (int i = 0; i < adapter.getCount(); i++) {
                    lvGigsFragment.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                actionModeRef = null;
                toolbar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        this.gigsList = new ArrayList<>();
        for (DataSnapshot child : dataSnapshot.getChildren()) {
            Gig gig = child.getValue(Gig.class);
            gigsList.add(gig);
        }
        this.adapter = new GigsAdapter(gigsList);
        this.lvGigsFragment.setAdapter(adapter);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
    }

    @OnItemClick(R.id.lvGigsFragment)
    public void showGigOnMap(int position) {
        Gig gig = (Gig) lvGigsFragment.getAdapter().getItem(position);
        Intent intent = new Intent(getContext(), GigDetailsActivity.class);
        intent.putExtra(GigDetailsActivity.KEY_GIG_OBJECT, gig);
        this.startActivity(intent);
    }

    @OnClick(R.id.btnAddNewGig)
    public void addNewGig() {
        Intent intent = new Intent(getContext(), NewGigActivity.class);
        startActivity(intent);
    }

    @OnItemLongClick(R.id.lvGigsFragment)
    public boolean deleteGigConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.dialog_title_delete));
        builder.setMessage(getString(R.string.dialog_msg_delete_gig_confirm));
        builder.setPositiveButton(getString(R.string.dialog_btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteGigs();
            }
        });
        builder.setNegativeButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
        return true;
    }

    private void deleteGigs() {
        this.dbRefGigs = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_gigs));
        SparseBooleanArray selected = adapter.getSelectedGigsIds();

        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {
                Gig gig = (Gig) adapter.getItem(selected.keyAt(i));
                adapter.removeGig(gig);
                this.dbRefGigs.child(gig.getmId()).removeValue();
            }
        }
        dismissCAB();
    }

    private void dismissCAB() {
        if (this.actionModeRef != null) {
            this.actionModeRef.finish();
        }
        this.toolbar.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btnShowAllOnMap)
    public void showAllOnMap() {
        if (this.gigsList != null && !this.gigsList.isEmpty()) {
            Intent intent = new Intent(getContext(), MapActivity.class);
            intent.putExtra(MapActivity.KEY_GIGS_LIST, (Serializable) this.gigsList);
            this.startActivity(intent);
        } else {
            Toast.makeText(getContext(), getString(R.string.toast_error_no_gigs_to_show), Toast.LENGTH_LONG).show();
        }
    }
}

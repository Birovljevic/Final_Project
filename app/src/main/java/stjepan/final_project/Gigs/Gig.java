package stjepan.final_project.Gigs;

import java.io.Serializable;

/**
 * Created by Stjepan on 25.2.2018..
 */

public class Gig implements Serializable {

    private String mId;
    private String mTitle;
    private String mAddress;
    private String mLatitude;
    private String mLongitude;
    private String mNotes;
    private String mDate;

    public Gig() {
    }

    public Gig(String mId, String mTitle, String mAddress, String mLatitude, String mLongitude, String mNotes, String mDate) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mAddress = mAddress;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.mNotes = mNotes;
        this.mDate = mDate;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmNotes() {
        return mNotes;
    }

    public void setmNotes(String mNotes) {
        this.mNotes = mNotes;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }
}
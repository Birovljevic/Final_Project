package stjepan.final_project.Gigs;

import android.support.v4.content.ContextCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.Artists.ArtistAdapter;
import stjepan.final_project.Lyrics.Lyric;
import stjepan.final_project.R;

/**
 * Created by Stjepan on 25.2.2018..
 */

public class GigsAdapter extends BaseAdapter {

    private List<Gig> mGigsList;
    private SparseBooleanArray mSelectedGigsIds;

    public GigsAdapter() {
        this.mGigsList = new ArrayList<>();
        this.mSelectedGigsIds = new SparseBooleanArray();
    }

    public GigsAdapter(List<Gig> gigs) {
        this.mGigsList = sortListAlpha(gigs);
        this.mSelectedGigsIds = new SparseBooleanArray();
    }

    @Override
    public int getCount() {
        return this.mGigsList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mGigsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        GigHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_gigs, parent, false);
            holder = new GigHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (GigHolder) convertView.getTag();
        }

        Gig result = this.mGigsList.get(position);

        holder.tvGigListTitle.setText(result.getmTitle());
        holder.tvGigsListAddress.setText(result.getmAddress());
        holder.tvGigsListDate.setText(result.getmDate());

        Picasso.with(parent.getContext())
                .load(R.drawable.gigs_list_icon)
                .fit()
                .centerCrop()
                .into(holder.ivGigsListIcon);
        return convertView;
    }

    static class GigHolder {
        @BindView(R.id.tvGigsListTitle)
        TextView tvGigListTitle;
        @BindView(R.id.tvGigsListAddress)
        TextView tvGigsListAddress;
        @BindView(R.id.tvGigsListDate)
        TextView tvGigsListDate;
        @BindView(R.id.ivGigsListIcon)
        ImageView ivGigsListIcon;
        @BindView(R.id.rlListItemGigs)
        RelativeLayout rlListItemGigs;

        public GigHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private List<Gig> sortListAlpha(List<Gig> gigList) {
        if (!gigList.isEmpty()) {
            Collections.sort(gigList, new Comparator<Gig>() {
                @Override
                public int compare(Gig g1, Gig g2) {
                    return g1.getmTitle().compareTo(g2.getmTitle());
                }
            });
        }
        return gigList;
    }

    public void removeGig(Gig gig) {
        this.mGigsList.remove(gig);
        notifyDataSetChanged();
    }

    public void setGigsList(List<Gig> gigList) {
        this.mGigsList = sortListAlpha(gigList);
        notifyDataSetChanged();
    }

    public List<Gig> getGigsList() {
        return this.mGigsList;
    }

    public void toggleSelection(int position) {
        boolean oldValue = this.mSelectedGigsIds.get(position);
        boolean newValue = !oldValue;
        if (newValue) {
            this.mSelectedGigsIds.put(position, newValue);
        } else {
            this.mSelectedGigsIds.delete(position);
        }
        notifyDataSetChanged();
    }

    public void removeSelected() {
        this.mSelectedGigsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public int getSelectedGigIdsCount() {
        return this.mSelectedGigsIds.size();
    }

    public boolean isSelected(int position) {
        toggleSelection(position);
        return this.mSelectedGigsIds.get(position);
    }

    public SparseBooleanArray getSelectedGigsIds() {
        return this.mSelectedGigsIds;
    }
}

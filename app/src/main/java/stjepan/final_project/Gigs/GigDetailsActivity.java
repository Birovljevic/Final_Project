package stjepan.final_project.Gigs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import stjepan.final_project.Maps.MapActivity;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class GigDetailsActivity extends Activity {

    public static final String KEY_GIG_OBJECT = "gigObject";
    private Gig gig;

    @BindView(R.id.tvGigTitleDetails)
    TextView tvGigTitleDetails;
    @BindView(R.id.tvGigAddressDetails)
    TextView tvGigAddressDetails;
    @BindView(R.id.tvGigNotesDetails)
    TextView tvGigNotesDetails;
    @BindView(R.id.tvGigDateDetails)
    TextView tvGigDateDetails;
    @BindView(R.id.btnUpdateGigDetails)
    Button btnUpdateGigDetails;
    @BindView(R.id.tvGigNotesDetailsTitle)
    TextView tvGigNotesDetailsTitle;
    @BindView(R.id.btnUpdateGigBack)
    Button btnUpdateGigBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gig_details);
        ButterKnife.bind(this);

        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }

    private void handleExtraData(Intent startingIntent) {
        if (startingIntent.hasExtra(KEY_GIG_OBJECT)) {
            this.gig = (Gig) startingIntent.getSerializableExtra(KEY_GIG_OBJECT);
            this.initializeUI();
        }
    }

    private void initializeUI() {
        this.tvGigTitleDetails.setText(gig.getmTitle());
        this.tvGigAddressDetails.setText(gig.getmAddress());
        this.tvGigDateDetails.setText(gig.getmDate());
        if (gig.getmNotes() != null && !gig.getmNotes().isEmpty()) {
            this.tvGigNotesDetails.setText(gig.getmNotes());
        } else {
            this.tvGigNotesDetails.setVisibility(View.GONE);
            this.tvGigNotesDetailsTitle.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnGigShowOnMapDetails)
    public void showGigOnMap() {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra(GigDetailsActivity.KEY_GIG_OBJECT, gig);
        this.startActivity(intent);
    }

    @OnClick(R.id.btnUpdateGigDetails)
    public void showGigUpdateActivity() {
        Intent intent = new Intent(this, GigEditActivity.class);
        intent.putExtra(GigEditActivity.KEY_GIG_OBJECT_EDIT, gig);
        this.startActivity(intent);
        finish();
    }

    @OnClick(R.id.btnUpdateGigBack)
    public void goToFragment() {
        Utils.goToFragment(this, GigsFragment.TITLE);
        finish();
    }
}

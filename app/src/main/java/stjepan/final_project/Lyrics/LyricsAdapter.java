package stjepan.final_project.Lyrics;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.final_project.R;

/**
 * Created by Stjepan on 13.2.2018..
 */

public class LyricsAdapter extends BaseAdapter {
    private List<Lyric> mLyricsList;
    SparseBooleanArray mSelectedLyricsIds;

    private LyricsAdapter() {
        this.mLyricsList = new ArrayList<>();
        this.mSelectedLyricsIds = new SparseBooleanArray();
    }

    public LyricsAdapter(List<Lyric> lyrics) {
        this.mSelectedLyricsIds = new SparseBooleanArray();
        this.mLyricsList = sortListAlpha(lyrics);
    }

    private List<Lyric> sortListAlpha(List<Lyric> lyricsList) {
        if (!lyricsList.isEmpty()) {
            Collections.sort(lyricsList, new Comparator<Lyric>() {
                @Override
                public int compare(Lyric l1, Lyric l2) {
                    return l1.getmTitle().compareTo(l2.getmTitle());
                }
            });
        }
        return lyricsList;
    }

    @Override
    public int getCount() {
        return this.mLyricsList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mLyricsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LyricHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_lyrics, parent, false);
            holder = new LyricHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (LyricHolder) convertView.getTag();
        }

        Lyric result = this.mLyricsList.get(position);

        holder.tvLyricsListTitle.setText(result.getmTitle());
        holder.tvLyricsListArtist.setText(result.getmArtistName());
        Picasso.with(parent.getContext())
                .load(R.drawable.lyrics_list_icon)
                .fit()
                .centerCrop()
                .into(holder.ivLyricsListIcon);
        return convertView;
    }

    static class LyricHolder {
        @BindView(R.id.tvLyricsListTitle)
        TextView tvLyricsListTitle;
        @BindView(R.id.tvLyricsListArtist)
        TextView tvLyricsListArtist;
        @BindView(R.id.ivLyricsListIcon)
        ImageView ivLyricsListIcon;
        @BindView(R.id.rlListItemLyrics)
        RelativeLayout rlListItemLyrics;

        public LyricHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void removeLyric(Lyric lyric) {
        this.mLyricsList.remove(lyric);
        notifyDataSetChanged();
    }

    public void setLyricsList(List<Lyric> lyricList) {
        this.mLyricsList = sortListAlpha(lyricList);
        notifyDataSetChanged();
    }

    public List<Lyric> getLyricsList() {
        return this.mLyricsList;
    }

    public void toggleSelection(int position) {
        boolean oldValue = this.mSelectedLyricsIds.get(position);
        boolean newValue = !oldValue;
        if (newValue) {
            this.mSelectedLyricsIds.put(position, newValue);
        } else {
            this.mSelectedLyricsIds.delete(position);
        }
        notifyDataSetChanged();
    }

    public void removeSelected() {
        this.mSelectedLyricsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public int getSelectedLyricsIdsCount() {
        return this.mSelectedLyricsIds.size();
    }

    public boolean isSelected(int position) {
        toggleSelection(position);
        return this.mSelectedLyricsIds.get(position);
    }

    public SparseBooleanArray getSelectedLyricsIds() {
        return this.mSelectedLyricsIds;
    }
}

package stjepan.final_project.Lyrics;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.Artists.ArtistDetailsActivity;
import stjepan.final_project.MainActivity;
import stjepan.final_project.R;
import stjepan.final_project.Repertoires.Repertoire;
import stjepan.final_project.Repertoires.RepertoireDetailsActivity;
import stjepan.final_project.Utils;
import stjepan.final_project.YouTube.YouTubeSearchActivity;

public class NewLyricActivity extends Activity {

    public static final String KEY_CALLED_FROM = "calledFrom";
    public static final String KEY_ARTIST_ID = "artistId";
    public static final String KEY_ARTIST_NAME = "artistName";
    private static final String LYRIC_SEARCH_BASE_URL = "http://www.google.com/#q=";

    private String calledFrom = "";
    private String artistId = "";
    private Lyric newLyric;

    private DatabaseReference dbRefLyrics;
    private DatabaseReference dbRefArtists;
    private DatabaseReference dbRefmLyrics;

    private SharedPreferences.Editor editor;

    private static final int REQUEST_YOUTUBE_SEARCH = 10;

    @BindView(R.id.etLyricTitle)
    EditText etLyricTitle;
    @BindView(R.id.etLyricArtist)
    EditText etLyricArtist;
    @BindView(R.id.etLyricText)
    EditText etLyricText;
    @BindView(R.id.etYouTubeUrl)
    EditText etYouTubeUrl;
    @BindView(R.id.btnSaveLyric)
    Button btnSaveLyric;
    @BindView(R.id.btnCancelLyricSaving)
    Button btnCancelLyricSaving;
    @BindView(R.id.ivYouTubeLogo)
    ImageView ivYouTubeLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_lyric);
        ButterKnife.bind(this);

        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));
        this.dbRefArtists = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_artists));
        this.dbRefmLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_mLyricsId));

        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.editor = getSharedPreferences("myPrefs", MODE_PRIVATE).edit();
        Intent startingIntent = this.getIntent();
        handlePreferences(startingIntent);
    }

    private void handleExtraData(Intent startingIntent) {

        if (startingIntent.hasExtra(this.KEY_CALLED_FROM)) {
            this.calledFrom = startingIntent.getStringExtra(this.KEY_CALLED_FROM);
        }
        if (startingIntent.hasExtra(this.KEY_ARTIST_ID)) {
            this.artistId = startingIntent.getStringExtra(this.KEY_ARTIST_ID);
        }
        if (startingIntent.hasExtra(this.KEY_ARTIST_NAME)) {
            String artistName = startingIntent.getStringExtra(this.KEY_ARTIST_NAME);
            this.etLyricArtist.setText(artistName);
        }
    }

    private void handlePreferences(Intent startingIntent) {
        Log.d("whats", "handlePreferences");
        SharedPreferences prefs = getSharedPreferences("myPrefs", MODE_PRIVATE);
        String restoredText = prefs.getString("text", null);
        if (restoredText != null) {
            String title = prefs.getString("title", "Empty");
            String artist = prefs.getString("artist", "Empty");
            String text = prefs.getString("text", "Empty");
            this.etLyricTitle.setText(title);
            this.etLyricArtist.setText(artist);
            if (!text.equals("Empty")) {
                this.etLyricText.setText(text);
            }
        }

        String action = startingIntent.getAction();
        String type = startingIntent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = startingIntent.getStringExtra(Intent.EXTRA_TEXT);
                this.etLyricText.setText(sharedText);

            }
        }
    }

    @OnClick(R.id.btnSaveLyric)
    public void saveLyric() {
        String title = this.etLyricTitle.getText().toString().trim();
        String artist = this.etLyricArtist.getText().toString().trim();
        String text = this.etLyricText.getText().toString().trim();
        String youTubeUrl = this.etYouTubeUrl.getText().toString().trim();

        if (title == null || title.isEmpty() || artist == null || artist.isEmpty() || text == null || text.isEmpty()) {
            Toast.makeText(this, getString(R.string.toast_error_title_artist_text_are_empty), Toast.LENGTH_LONG).show();
        } else {
            newLyric = new Lyric();
            newLyric.setmId(dbRefLyrics.push().getKey());
            newLyric.setmTitle(Utils.capitalize(title));
            newLyric.setmArtistName(Utils.capitalize(artist));
            newLyric.setmText(text);
            if (youTubeUrl != null && !youTubeUrl.isEmpty()) {
                newLyric.setmYouTubeUrl(youTubeUrl);
            }

            dbRefArtists.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<Artist> artistList = new ArrayList<>();

                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Artist artistTest = child.getValue(Artist.class);
                        artistList.add(artistTest);
                    }
                    //provjeri postoji li vec artist s tim imenom i ako da, dodaj mu lyric i azuriraj ga
                    boolean matchFound = false;
                    int i = 0;
                    if (!artistList.isEmpty()) {
                        do {
                            if ((artistList.get(i).getmName()).equals(newLyric.getmArtistName())) {
                                matchFound = true;
                                Artist artistToUpdate = artistList.get(i);

                                LyricShort lyricShort = new LyricShort(newLyric.getmId(), newLyric.getmTitle());
                                //spremi lyric nakon sto si uzeo artistID
                                newLyric.setmArtistId(artistToUpdate.getmId());
                                dbRefLyrics.child(newLyric.getmId()).setValue(newLyric);

                                //dohvati i azuriraj listu lirika artista
                                if (artistToUpdate.getmLyrics() == null) {
                                    artistToUpdate.setmLyrics(new ArrayList<LyricShort>());
                                }
                                List<LyricShort> listLyrics = artistToUpdate.getmLyrics();
                                listLyrics.add(lyricShort);
                                artistToUpdate.setmLyrics(listLyrics);

                                dbRefArtists.child(artistToUpdate.getmId()).setValue(artistToUpdate);
                            }
                            i++;
                        } while (!matchFound == true && !(i >= artistList.size()));
                    }
                    if (matchFound == false) {
                        Artist newArtist = new Artist();
                        newArtist.setmId(dbRefArtists.push().getKey());
                        newArtist.setmName(newLyric.getmArtistName());
                        LyricShort lyricShort = new LyricShort(newLyric.getmId(), newLyric.getmTitle());
                        List<LyricShort> lista = new ArrayList<>();
                        lista.add(lyricShort);
                        newArtist.setmLyrics(lista);
                        dbRefArtists.child(newArtist.getmId()).setValue(newArtist);
                        newLyric.setmArtistId(newArtist.getmId());
                        dbRefLyrics.child(newLyric.getmId()).setValue(newLyric);
                    }
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_lyric_successfully_saved), Toast.LENGTH_SHORT).show();
                    goToLyricsDetails();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    @OnClick(R.id.btnCancelLyricSaving)
    public void changeActivity() {
        clearPreferences();
        if (checkIfCanGoToArtistDetails()) {
            goToArtistDetails();
        } else {
            goToMain();
        }
    }

    private void goToLyricsDetails() {
        Intent intent = new Intent(this, LyricDetailsActivity.class);
        intent.putExtra(LyricDetailsActivity.KEY_LYRIC_OBJECT, newLyric);
        startActivity(intent);
        finish();
    }

    private void goToMain() {
        Utils.goToFragment(this, LyricsFragment.TITLE);
        finish();
    }

    private boolean checkIfCanGoToArtistDetails() {
        if (this.calledFrom != null && !this.calledFrom.isEmpty() &&
                this.calledFrom.equals(ArtistDetailsActivity.ACTIVITY_NAME) &&
                this.artistId != null && !this.artistId.isEmpty()) {
            return true;
        } else
            return false;
    }

    private void goToArtistDetails() {
        Intent intent = new Intent();
        intent.setClass(this, ArtistDetailsActivity.class);
        intent.putExtra(ArtistDetailsActivity.KEY_ARTIST_ID, this.artistId);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        this.changeActivity();
    }

    @OnClick(R.id.ivYouTubeLogo)
    public void searchYouTubeLink() {
        String artist = this.etLyricArtist.getText().toString().trim();
        String title = this.etLyricTitle.getText().toString().trim();

        if (artist != null && !artist.isEmpty() && title != null && !title.isEmpty()) {
            String searchTerm = artist + " " + title;
            Intent youtubeSearchIntent = new Intent(this, YouTubeSearchActivity.class);
            youtubeSearchIntent.putExtra(YouTubeSearchActivity.SEARCH_TERM, searchTerm);
            this.startActivityForResult(youtubeSearchIntent, REQUEST_YOUTUBE_SEARCH);
        } else {
            Toast.makeText(this, "toast_error_enter_artist_and_title", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_YOUTUBE_SEARCH && resultCode == RESULT_OK) {
            if (data.hasExtra(YouTubeSearchActivity.YOUTUBE_LINK)) {
                String youtubeId = data.getStringExtra(YouTubeSearchActivity.YOUTUBE_LINK);
                this.etYouTubeUrl.setText(youtubeId);
            }
        }
    }

    @OnClick(R.id.tvNewLyricSearchText)
    public void searchLyricText() {
        String artist = this.etLyricArtist.getText().toString().trim();
        String title = this.etLyricTitle.getText().toString().trim();

        if (artist != null && !artist.isEmpty() && title != null && !title.isEmpty()) {
            String query = artist + " " + title;
            Uri uri = Uri.parse(this.LYRIC_SEARCH_BASE_URL + query);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);

            sharedPreferences();
            startActivity(intent);
        } else
            Toast.makeText(this, "toast_error_enter_artist_and_title", Toast.LENGTH_LONG).show();
    }

    private void sharedPreferences() {
        editor.putString("title", this.etLyricTitle.getText().toString().trim());
        editor.putString("artist", this.etLyricArtist.getText().toString().trim());
        editor.putString("text", this.etLyricText.getText().toString().trim());
        editor.apply();
    }

    private void clearPreferences() {
        this.editor.clear();
        this.editor.apply();
    }
}

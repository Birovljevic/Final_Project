package stjepan.final_project.Lyrics;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.MainActivity;
import stjepan.final_project.R;
import stjepan.final_project.Utils;
import stjepan.final_project.YouTube.YouTubeSearchActivity;

public class LyricUpdateActivity extends Activity {

    public static final String KEY_LYRIC_OBJECT = "lyricObject";
    public static final String KEY_ARTIST_ID = "artistId";
    private static final String LYRIC_SEARCH_BASE_URL = "http://www.google.com/#q=";
    private DatabaseReference dbRefLyrics, dbRefArtists;

    private Lyric oldLyric;
    private Lyric updatedLyric;
    private String artistId;

    private SharedPreferences.Editor editorUpdate;

    private static final int REQUEST_YOUTUBE_SEARCH = 10;

    @BindView(R.id.etLyricUpdateTitle)
    TextView etLyricUpdateTitle;
    @BindView(R.id.etLyricUpdateArtistName)
    TextView etLyricUpdateArtistName;
    @BindView(R.id.etLyricUpdateText)
    TextView etLyricUpdateText;
    @BindView(R.id.btnSaveUpdatedLyric)
    Button btnSaveUpdatedLyric;
    @BindView(R.id.btnCancelUpdatedLyric)
    Button btnCancelUpdatedLyric;
    @BindView(R.id.tvLyricUpdateSearchText)
    TextView btnLyricUpdateSearchText;
    @BindView(R.id.ivLyricUpdateYTLogo)
    ImageView ivLyricUpdateYTLogo;
    @BindView(R.id.etLyricUpdateYTUrl)
    EditText etLyricUpdateYTUrl;
    @BindView(R.id.tvLyricUpdateSearchYT)
    TextView tvLyricUpdateSearchYT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lyric_update);
        ButterKnife.bind(this);

        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));
        this.dbRefArtists = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_artists));

        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.editorUpdate = getSharedPreferences("myPrefsUpdate", MODE_PRIVATE).edit();
        Intent startingIntent = this.getIntent();
        handlePreferences(startingIntent);
    }

    private void handleExtraData(Intent startingIntent) {

        if (startingIntent.hasExtra(this.KEY_LYRIC_OBJECT)) {
            this.oldLyric = (Lyric) startingIntent.getSerializableExtra(this.KEY_LYRIC_OBJECT);
            this.initializeUI();
        }
        if (startingIntent.hasExtra(this.KEY_ARTIST_ID)) {
            this.artistId = startingIntent.getStringExtra(this.KEY_ARTIST_ID);
            this.initializeUI();
        }
    }

    private void initializeUI() {
        this.etLyricUpdateTitle.setText(oldLyric.getmTitle());
        this.etLyricUpdateArtistName.setText(oldLyric.getmArtistName());
        this.etLyricUpdateText.setText(oldLyric.getmText());
        this.etLyricUpdateYTUrl.setText(oldLyric.getmYouTubeUrl());
    }

    private void handlePreferences(Intent startingIntent) {
        SharedPreferences prefs = getSharedPreferences("myPrefsUpdate", MODE_PRIVATE);
        String restoredText = prefs.getString("text", null);
        if (restoredText != null) {
            String title = prefs.getString("title", "Empty");
            String artist = prefs.getString("artist", "Empty");
            String text = prefs.getString("text", "Empty");
            oldLyric = new Lyric();
            oldLyric.setmId(prefs.getString("oldLyricId", "Empty"));
            oldLyric.setmArtistId(prefs.getString("oldLyricArtistId", "Empty"));
            oldLyric.setmArtistName(prefs.getString("oldLyricArtistName", "Empty"));

            this.etLyricUpdateTitle.setText(title);
            this.etLyricUpdateArtistName.setText(artist);
            this.etLyricUpdateText.setText(text);
        }

        String action = startingIntent.getAction();
        String type = startingIntent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = startingIntent.getStringExtra(Intent.EXTRA_TEXT);
                this.etLyricUpdateText.setText(sharedText);
            }
        }
    }

    @OnClick({R.id.ivLyricUpdateYTLogo, R.id.tvLyricUpdateSearchYT})
    public void searchYTvideo() {
        String artist = this.etLyricUpdateArtistName.getText().toString().trim();
        String title = this.etLyricUpdateTitle.getText().toString().trim();
        if (artist != null && !artist.isEmpty() && title != null && !title.isEmpty()) {
            String searchTerm = artist + " " + title;
            Intent youtubeSearchIntent = new Intent(this, YouTubeSearchActivity.class);
            youtubeSearchIntent.putExtra(YouTubeSearchActivity.SEARCH_TERM, searchTerm);
            this.startActivityForResult(youtubeSearchIntent, REQUEST_YOUTUBE_SEARCH);
        } else
            Toast.makeText(this, getString(R.string.toast_error_enter_artist_and_title), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_YOUTUBE_SEARCH && resultCode == RESULT_OK) {
            if (data.hasExtra(YouTubeSearchActivity.YOUTUBE_LINK)) {
                String youtubeId = data.getStringExtra(YouTubeSearchActivity.YOUTUBE_LINK);
                this.etLyricUpdateYTUrl.setText(youtubeId);
            }
        }
    }

    @OnClick(R.id.tvLyricUpdateSearchText)
    public void searchLyricText() {
        String artist = this.etLyricUpdateArtistName.getText().toString().trim();
        String title = this.etLyricUpdateTitle.getText().toString().trim();
        if (artist != null && !artist.isEmpty() && title != null && !title.isEmpty()) {
            String query = artist + " " + title;
            Uri uri = Uri.parse(this.LYRIC_SEARCH_BASE_URL + query);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            clearPreferences();
            sharedPreferences();
            startActivity(intent);
        } else
            Toast.makeText(this, getString(R.string.toast_error_enter_artist_and_title), Toast.LENGTH_LONG).show();
    }

    private void sharedPreferences() {
        editorUpdate.putString("title", Utils.capitalize(this.etLyricUpdateTitle.getText().toString().trim()));
        editorUpdate.putString("artist", Utils.capitalize(this.etLyricUpdateArtistName.getText().toString().trim()));
        editorUpdate.putString("text", this.etLyricUpdateText.getText().toString().trim());
        editorUpdate.putString("oldLyricId", oldLyric.getmId());
        editorUpdate.putString("oldLyricArtistId", oldLyric.getmArtistId());
        editorUpdate.putString("oldLyricArtistName", oldLyric.getmArtistName());
        editorUpdate.apply();
    }

    @OnClick(R.id.btnSaveUpdatedLyric)
    public void saveUpdatedLyric() {
        String title = etLyricUpdateTitle.getText().toString().trim();
        String artistName = etLyricUpdateArtistName.getText().toString().trim();
        String text = etLyricUpdateText.getText().toString().trim();
        if (title == null || title.isEmpty() || artistName == null || artistName.isEmpty() || text == null || text.isEmpty()) {
            Toast.makeText(this, getString(R.string.toast_error_title_artist_text_are_empty), Toast.LENGTH_LONG).show();
        } else {
            updatedLyric = new Lyric();
            updatedLyric.setmId(oldLyric.getmId());
            updatedLyric.setmTitle(Utils.capitalize(title));
            updatedLyric.setmText(text);
            updatedLyric.setmArtistName(Utils.capitalize(artistName));

            String youTubeUrl = this.etLyricUpdateYTUrl.getText().toString().trim();
            if (youTubeUrl != null && !youTubeUrl.isEmpty()) {
                updatedLyric.setmYouTubeUrl(youTubeUrl);
            } else {
                if (oldLyric.getmYouTubeUrl() != null && !oldLyric.getmYouTubeUrl().isEmpty()) {
                    updatedLyric.setmYouTubeUrl(oldLyric.getmYouTubeUrl());
                }
            }
            //ako se nije mijenjao artist
            if (updatedLyric.getmArtistName().equals(oldLyric.getmArtistName())) {
                updateLyricAristUnchanged();
                //ako se mijenjao artist
            } else {
                this.dbRefArtists.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Artist newArtist = new Artist();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Artist artist = child.getValue(Artist.class);
                            if (artist.getmName().equals(updatedLyric.getmArtistName())) {
                                newArtist = artist;
                            }
                        }
                        //imamo artista u bazi
                        if (!(newArtist.getmId() == null)) {

                            updatedLyric.setmArtistId(newArtist.getmId());
                            dbRefLyrics.child(updatedLyric.getmId()).setValue(updatedLyric);

                            //novi artist azurira listu
                            LyricShort newLyricShort = new LyricShort(updatedLyric.getmId(), updatedLyric.getmTitle());
                            if (newArtist.getmLyrics() == null) {
                                newArtist.setmLyrics(new ArrayList<LyricShort>());
                            }
                            newArtist.getmLyrics().add(newLyricShort);
                            dbRefArtists.child(newArtist.getmId()).setValue(newArtist);

                            //stari artist brise lyric iz liste
                            dbRefArtists.child(oldLyric.getmArtistId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Artist oldArtist = dataSnapshot.getValue(Artist.class);
                                    List<LyricShort> updatedList = new ArrayList<>();
                                    if (oldArtist != null) {
                                        if (oldArtist.getmLyrics() != null) {
                                            List<LyricShort> lista = oldArtist.getmLyrics();
                                            for (LyricShort lyric : lista) {
                                                if (!lyric.getmLyricId().equals(updatedLyric.getmId())) {
                                                    updatedList.add(lyric);
                                                }
                                            }
                                            oldArtist.setmLyrics(updatedList);
                                            dbRefArtists.child(oldArtist.getmId()).setValue(oldArtist);
                                            Toast.makeText(LyricUpdateActivity.this, getString(R.string.toast_lyric_successfully_update), Toast.LENGTH_SHORT).show();
                                            goToLyricDetails();
                                            //ako stari artist ostane bez lyrcs-a, obrisi ga
                                        } else if (oldArtist.getmLyrics() == null || oldArtist.getmLyrics().isEmpty()) {
                                            dbRefArtists.child(oldArtist.getmId()).removeValue();
                                            Toast.makeText(LyricUpdateActivity.this, getString(R.string.toast_lyric_successfully_update), Toast.LENGTH_SHORT).show();
                                            goToLyricDetails();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                            //nema artista u bazi
                        } else {
                            Artist brandNewArtist = new Artist();
                            brandNewArtist.setmId(dbRefArtists.push().getKey());
                            brandNewArtist.setmName(updatedLyric.getmArtistName());
                            LyricShort newLyShort = new LyricShort(updatedLyric.getmId(), updatedLyric.getmTitle());
                            List<LyricShort> listaLyShort = new ArrayList<>();
                            listaLyShort.add(newLyShort);
                            brandNewArtist.setmLyrics(listaLyShort);
                            dbRefArtists.child(brandNewArtist.getmId()).setValue(brandNewArtist);

                            updatedLyric.setmArtistId(brandNewArtist.getmId());
                            dbRefLyrics.child(updatedLyric.getmId()).setValue(updatedLyric);

                            dbRefArtists.child(oldLyric.getmArtistId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Artist oldArtist = dataSnapshot.getValue(Artist.class);
                                    List<LyricShort> updatedList = new ArrayList<>();
                                    if (oldArtist != null) {
                                        if (oldArtist.getmLyrics() != null) {
                                            List<LyricShort> lista = oldArtist.getmLyrics();
                                            for (LyricShort lyric : lista) {
                                                if (!lyric.getmLyricId().equals(updatedLyric.getmId())) {
                                                    updatedList.add(lyric);
                                                }
                                            }
                                            oldArtist.setmLyrics(updatedList);
                                            dbRefArtists.child(oldArtist.getmId()).setValue(oldArtist);
                                            Toast.makeText(LyricUpdateActivity.this, getString(R.string.toast_lyric_successfully_update), Toast.LENGTH_SHORT).show();
                                            goToLyricDetails();
                                            //Ako stari artist ostane bez lyrcs-a, obrisi ga
                                        } else if (oldArtist.getmLyrics() == null || oldArtist.getmLyrics().isEmpty()) {
                                            dbRefArtists.child(oldArtist.getmId()).removeValue();
                                            Toast.makeText(LyricUpdateActivity.this, getString(R.string.toast_lyric_successfully_update), Toast.LENGTH_SHORT).show();
                                            goToLyricDetails();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    private void updateLyricAristUnchanged() {
        updatedLyric.setmArtistId(oldLyric.getmArtistId());
        //azuriraj listu lyrica u artist nodu
        this.dbRefArtists.child(updatedLyric.getmArtistId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Artist artistToUpdate = dataSnapshot.getValue(Artist.class);
                List<LyricShort> updatedList = new ArrayList<>();

                if (artistToUpdate != null) {
                    LyricShort oldLyricShort = new LyricShort(oldLyric.getmId(), oldLyric.getmTitle());
                    if (artistToUpdate.getmLyrics() == null) {
                        artistToUpdate.setmLyrics(new ArrayList<LyricShort>());
                    } else {
                        List<LyricShort> lista = artistToUpdate.getmLyrics();
                        for (LyricShort lyric : lista) {
                            if (!lyric.getmLyricId().equals(oldLyricShort.getmLyricId())) {
                                updatedList.add(lyric);
                            }
                        }
                    }
                    LyricShort newLyricShort = new LyricShort(updatedLyric.getmId(), updatedLyric.getmTitle());
                    updatedList.add(newLyricShort);
                    artistToUpdate.setmLyrics(updatedList);

                    dbRefArtists.child(artistToUpdate.getmId()).setValue(artistToUpdate);
                    dbRefLyrics.child(updatedLyric.getmId()).setValue(updatedLyric);
                    Toast.makeText(LyricUpdateActivity.this, getString(R.string.toast_lyric_successfully_update), Toast.LENGTH_SHORT).show();
                    goToLyricDetails();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @OnClick(R.id.btnCancelUpdatedLyric)
    public void goToLyricDetails() {
        clearPreferences();
        Intent intent = new Intent(this, LyricDetailsActivity.class);
        LyricShort lyricShort = new LyricShort();
        lyricShort.setmLyricId(oldLyric.getmId());
        intent.putExtra(LyricDetailsActivity.KEY_LYRIC_SHORT_OBJECT, lyricShort);
        if (this.artistId != null && !this.artistId.isEmpty()) {
            intent.putExtra(LyricDetailsActivity.KEY_CALLING_ARTIST_ID, this.artistId);
        }
        startActivity(intent);
        finish();
    }

    private void clearPreferences() {
        this.editorUpdate.clear();
        this.editorUpdate.apply();
    }

    @Override
    public void onBackPressed() {
        this.goToLyricDetails();
    }
}

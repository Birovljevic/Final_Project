package stjepan.final_project.Lyrics;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.R;

public class LyricsFragment extends Fragment implements ValueEventListener {
    public static final String TITLE = "Lyrics";
    public static final String KEY_NAME = "lyrics";

    private DatabaseReference dbRefLyrics;
    private DatabaseReference dbRefArtists;

    @BindView(R.id.lvLyricsFragment)
    ListView lvLyricsFragment;
    @BindView(R.id.btnAddNewLyric)
    Button btnAddNewLyric;

    public LyricsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lyrics, container, false);
        ButterKnife.bind(this, view);
        this.dbRefArtists = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_artists));
        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));
        this.dbRefLyrics.addValueEventListener(this);
        return view;
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        List<Lyric> lyricList = new ArrayList<>();
        for (DataSnapshot child : dataSnapshot.getChildren()) {
            Lyric lyric = child.getValue(Lyric.class);
            lyricList.add(lyric);
        }
        LyricsAdapter adapter = new LyricsAdapter(lyricList);
        this.lvLyricsFragment.setAdapter(adapter);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
    }

    @OnClick(R.id.btnAddNewLyric)
    public void addNewLyric() {
        Intent intent = new Intent(getContext(), NewLyricActivity.class);
        intent.putExtra(NewLyricActivity.KEY_CALLED_FROM, "LyricsFragment");
        this.startActivity(intent);
    }

    @OnItemClick(R.id.lvLyricsFragment)
    public void lyricDetails(int position) {
        Lyric lyric = (Lyric) lvLyricsFragment.getAdapter().getItem(position);
        Intent intent = new Intent(getContext(), LyricDetailsActivity.class);
        intent.putExtra(LyricDetailsActivity.KEY_LYRIC_OBJECT, lyric);
        this.startActivity(intent);
    }

    @OnItemLongClick(R.id.lvLyricsFragment)
    public boolean deleteLyricConfirmation(final int position, View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.dialog_title_delete));
        builder.setMessage(R.string.dialog_msg_delete_lyric_confirm);
        builder.setPositiveButton(getText(R.string.dialog_btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteLyric(position);
            }
        });
        builder.setNegativeButton(getText(R.string.dialog_btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
        return true;
    }

    private void deleteLyric(int position) {
        final Lyric lyric = (Lyric) lvLyricsFragment.getAdapter().getItem(position);
        this.dbRefLyrics.child(lyric.getmId()).removeValue();
        this.dbRefArtists.child(lyric.getmArtistId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Artist artist = dataSnapshot.getValue(Artist.class);
                List<LyricShort> newList = new ArrayList<>();
                if (artist.getmLyrics() != null && !artist.getmLyrics().isEmpty()) {
                    for (LyricShort lyricShort : artist.getmLyrics()) {
                        if (!lyricShort.getmLyricId().equals(lyric.getmId())) {
                            newList.add(lyricShort);
                        }
                    }
                    if (newList.isEmpty()) {
                        dbRefArtists.child(artist.getmId()).removeValue();
                    } else {
                        artist.setmLyrics(newList);
                        dbRefArtists.child(artist.getmId()).setValue(artist);
                    }
                    Toast.makeText(getContext(), getText(R.string.toast_lyric_successfully_deleted), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}





package stjepan.final_project.Lyrics;

import java.io.Serializable;

/**
 * Created by Stjepan on 19.2.2018..
 */

public class LyricShort implements Serializable {

    private String mLyricId;
    private String mLyricTitle;

    public LyricShort() {

    }

    public LyricShort(String mLyricId, String mLyricTitle) {

        this.mLyricId = mLyricId;
        this.mLyricTitle = mLyricTitle;
    }


    public String getmLyricId() {
        return mLyricId;
    }

    public void setmLyricId(String mLyricId) {
        this.mLyricId = mLyricId;
    }

    public String getmLyricTitle() {
        return mLyricTitle;
    }

    public void setmLyricTitle(String mLyricTitle) {
        this.mLyricTitle = mLyricTitle;
    }
}

package stjepan.final_project.Lyrics;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.Artists.ArtistDetailsActivity;
import stjepan.final_project.MainActivity;
import stjepan.final_project.R;
import stjepan.final_project.Repertoires.Repertoire;
import stjepan.final_project.Repertoires.RepertoireDetailsActivity;
import stjepan.final_project.Utils;
import stjepan.final_project.YouTube.YouTubeConfig;

public class LyricDetailsActivity extends YouTubeBaseActivity {

    public static final String KEY_LYRIC_OBJECT = "lyricObject";
    public static final String KEY_LYRIC_SHORT_OBJECT = "lyricShortObject";
    public static final String KEY_CALLING_ARTIST_ID = "callingArtistId";
    public static final String KEY_CALLING_REPERTOIRE_OBJECT = "callingRepertoireObject";

    private DatabaseReference dbRefLyrics;
    private Lyric lyric;
    private String callingArtistId;
    private Repertoire callingRepertoireObject;

    @BindView(R.id.tvLyricDetailsTitle)
    TextView tvLyricDetailsTitle;
    @BindView(R.id.tvLyricDetailsArtist)
    TextView tvLyricDetailsArtist;
    @BindView(R.id.tvLyricDetailsText)
    TextView tvLyricDetailsText;
    @BindView(R.id.btnUpdateLyric)
    Button btnUpdateLyric;
    @BindView(R.id.btnUpdateLyricBack)
    Button btnUpdateLyricBack;
    @BindView(R.id.vYouTubePlayer)
    YouTubePlayerView vYouTubePlayer;
    @BindView(R.id.ivPlayYTVideo)
    ImageView ivPlayYTVideo;
    YouTubePlayer.OnInitializedListener mOnInitializedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lyric_details);
        ButterKnife.bind(this);

        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }

    private void handleExtraData(Intent startingIntent) {
        if (startingIntent.hasExtra(this.KEY_CALLING_REPERTOIRE_OBJECT)) {
            this.callingRepertoireObject = (Repertoire) startingIntent.getSerializableExtra(this.KEY_CALLING_REPERTOIRE_OBJECT);
        }
        if (startingIntent.hasExtra(this.KEY_CALLING_ARTIST_ID)) {
            this.callingArtistId = startingIntent.getStringExtra(this.KEY_CALLING_ARTIST_ID);
        }
        if (startingIntent.hasExtra(this.KEY_LYRIC_OBJECT)) {
            this.lyric = (Lyric) startingIntent.getSerializableExtra(this.KEY_LYRIC_OBJECT);
            this.initializeUI(lyric);
        } else if (startingIntent.hasExtra(this.KEY_LYRIC_SHORT_OBJECT)) {
            LyricShort lyricShort = (LyricShort) startingIntent.getSerializableExtra(KEY_LYRIC_SHORT_OBJECT);

            this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics)).child(lyricShort.getmLyricId());
            this.dbRefLyrics.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    lyric = dataSnapshot.getValue(Lyric.class);
                    if (lyric != null) {
                        initializeUI(lyric);
                    } else
                        Toast.makeText(LyricDetailsActivity.this, getString(R.string.toast_error_please_try_again), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void initializeUI(Lyric lyric) {
        this.tvLyricDetailsTitle.setText(lyric.getmTitle());
        this.tvLyricDetailsArtist.setText(lyric.getmArtistName());
        this.tvLyricDetailsText.setText(lyric.getmText());
    }

    @OnClick(R.id.btnUpdateLyric)
    public void updateLyric() {
        Intent intent = new Intent(this, LyricUpdateActivity.class);
        intent.putExtra(LyricUpdateActivity.KEY_LYRIC_OBJECT, this.lyric);
        if (this.callingArtistId != null && !this.callingArtistId.isEmpty()) {
            intent.putExtra(LyricUpdateActivity.KEY_ARTIST_ID, this.callingArtistId);
        }
        startActivity(intent);
        finish();
    }

    @OnClick({R.id.ivPlayYTVideo, R.id.vYouTubePlayer})
    public void playYoutubeVideo() {
        if (lyric.getmYouTubeUrl() != null && !lyric.getmYouTubeUrl().isEmpty()) {
            vYouTubePlayer.setVisibility(View.VISIBLE);
            //typed value converta iz pixela u dps, get.DisplayMetrics() vraca rezoluciju uredjaja
            float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) vYouTubePlayer.getLayoutParams();
            params.height = (int) pixels;
            vYouTubePlayer.setLayoutParams(params);

            mOnInitializedListener = new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                    youTubePlayer.loadVideo(lyric.getmYouTubeUrl());
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                    Toast.makeText(LyricDetailsActivity.this, getString(R.string.toast_error_please_try_again), Toast.LENGTH_LONG).show();
                }
            };
            vYouTubePlayer.initialize(YouTubeConfig.YOUTUBE_API_KEY, mOnInitializedListener);
        } else {
            Toast.makeText(this, getString(R.string.toast_lyric_does_not_contain_video), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        this.goToFragment();
    }

    @OnClick(R.id.btnUpdateLyricBack)
    public void goToFragment() {
        if (this.callingArtistId != null && !this.callingArtistId.isEmpty()) {
            Intent intent = new Intent(this, ArtistDetailsActivity.class);
            intent.putExtra(ArtistDetailsActivity.KEY_ARTIST_ID, this.callingArtistId);
            startActivity(intent);
            finish();
        } else if (this.callingRepertoireObject != null) {
            Intent intent = new Intent(this, RepertoireDetailsActivity.class);
            intent.putExtra(RepertoireDetailsActivity.KEY_REPERTOIRE_OBJECT, this.callingRepertoireObject);
            startActivity(intent);
            finish();
        } else {
            Utils.goToFragment(this, LyricsFragment.TITLE);
            finish();
        }
    }
}

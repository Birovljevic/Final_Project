package stjepan.final_project.Lyrics;

import java.io.Serializable;

/**
 * Created by Stjepan on 9.2.2018..
 */

public class Lyric implements Serializable {

    private String mId;
    private String mTitle;
    private String mArtistName;
    private String mArtistId;
    private String mText;
    private String mYouTubeUrl;

    public Lyric() {

    }

    public Lyric(String mId, String mTitle, String mArtistName, String mArtistId, String mText, String mYouTubeUrl) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mArtistName = mArtistName;
        this.mArtistId = mArtistId;
        this.mText = mText;
        this.mYouTubeUrl = mYouTubeUrl;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmArtistName() {
        return mArtistName;
    }

    public void setmArtistName(String mArtistName) {
        this.mArtistName = mArtistName;
    }

    public String getmArtistId() {
        return mArtistId;
    }

    public void setmArtistId(String mArtistId) {
        this.mArtistId = mArtistId;
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }

    public String getmYouTubeUrl() {
        return mYouTubeUrl;
    }

    public void setmYouTubeUrl(String mYouTubeUrl) {
        this.mYouTubeUrl = mYouTubeUrl;
    }
}


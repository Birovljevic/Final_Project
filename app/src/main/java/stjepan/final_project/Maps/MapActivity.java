package stjepan.final_project.Maps;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import stjepan.final_project.Gigs.Gig;
import stjepan.final_project.Lyrics.Lyric;
import stjepan.final_project.Lyrics.NewLyricActivity;
import stjepan.final_project.R;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mGoogleMap;
    MapFragment mMapFragment;

    public static final String KEY_GIG_OBJECT = "gigObject";
    public static final String KEY_GIGS_LIST = "gigsList";
    private Gig gig;
    private List<Gig> gigsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
        this.initialize();
    }

    private void handleExtraData(Intent startingIntent) {
        if (startingIntent.hasExtra(KEY_GIG_OBJECT)) {
            this.gig = (Gig) startingIntent.getSerializableExtra(KEY_GIG_OBJECT);
        }
        if (startingIntent.hasExtra(KEY_GIGS_LIST)) {
            Log.d("TAG", "ima extra lista");
            this.gigsList = (List<Gig>) startingIntent.getSerializableExtra(KEY_GIGS_LIST);
        }
    }

    private void initialize() {
        this.mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fGoogleMap);
        this.mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        UiSettings uiSettings = this.mGoogleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.mGoogleMap.setMyLocationEnabled(true);

        if (this.gigsList != null) {
            for (Gig gig : gigsList) {
                LatLng geoPoint = new LatLng(Double.valueOf(gig.getmLatitude()), Double.valueOf(gig.getmLongitude()));
                MarkerOptions newMarkerOptions = new MarkerOptions();
                newMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.gig_icon));
                newMarkerOptions.title(gig.getmTitle());
                newMarkerOptions.snippet(gig.getmDate());
                newMarkerOptions.position(geoPoint);
                mGoogleMap.addMarker(newMarkerOptions);
            }
        } else if (this.gig != null) {
            LatLng geoPoint = new LatLng(Double.valueOf(gig.getmLatitude()), Double.valueOf(gig.getmLongitude()));
            MarkerOptions newMarkerOptions = new MarkerOptions();
            newMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.gig_icon));
            newMarkerOptions.title(gig.getmTitle());
            newMarkerOptions.snippet(gig.getmDate());
            newMarkerOptions.position(geoPoint);
            mGoogleMap.addMarker(newMarkerOptions);
        }
    }
}

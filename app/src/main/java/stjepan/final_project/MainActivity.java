package stjepan.final_project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.final_project.Artists.ArtistsFragment;
import stjepan.final_project.Gigs.GigsFragment;
import stjepan.final_project.Lyrics.LyricsFragment;
import stjepan.final_project.Repertoires.Repertoire;
import stjepan.final_project.Repertoires.RepertoiresFragment;

import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_FRAGMENT_NAME = "fragmentName";
    private SharedPreferences.Editor editor, editorUpdate;

    @BindView(R.id.vpPager)
    ViewPager vpPager;
    @BindView(R.id.tlTabs)
    TabLayout tlTabs;
    @BindView(R.id.tMainToolbar)
    Toolbar tMainToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        this.editorUpdate = getSharedPreferences("myPrefsUpdate", MODE_PRIVATE).edit();
        this.editor = getSharedPreferences("myPrefs", MODE_PRIVATE).edit();

        this.clearPreferences();
        if (isNetworkAvailable()) {
            this.loadFragments();
            this.handleExtra(getIntent());
        } else {
            Toast.makeText(this, getString(R.string.toast_error_not_connected_to_the_network), Toast.LENGTH_LONG).show();
        }
    }

    private void handleExtra(Intent startingIntent) {
        if (startingIntent.hasExtra(MainActivity.KEY_FRAGMENT_NAME)) {
            this.vpPager.setCurrentItem(Integer.valueOf(startingIntent.getStringExtra(this.KEY_FRAGMENT_NAME)));
        }
    }

    private void clearPreferences() {
        this.editor.clear();
        this.editor.apply();
        this.editorUpdate.clear();
        this.editorUpdate.apply();
    }

    private void loadFragments() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.insertFragment(new LyricsFragment(), LyricsFragment.TITLE);
        adapter.insertFragment(new ArtistsFragment(), ArtistsFragment.TITLE);
        adapter.insertFragment(new RepertoiresFragment(), RepertoiresFragment.TITLE);
        adapter.insertFragment(new GigsFragment(), GigsFragment.TITLE);
        this.vpPager.setAdapter(adapter);
        this.tlTabs.setupWithViewPager(this.vpPager);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
}

package stjepan.final_project.Artists;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.final_project.Lyrics.LyricShort;
import stjepan.final_project.R;

/**
 * Created by Stjepan on 19.2.2018..
 */

public class LyricsShortAdapter extends BaseAdapter {
    private List<LyricShort> mLyricsList;
    private SparseBooleanArray mSelectedLyricsIds;

    public LyricsShortAdapter() {
        this.mLyricsList = new ArrayList<>();
        this.mSelectedLyricsIds = new SparseBooleanArray();
    }

    public LyricsShortAdapter(List<LyricShort> lyrics) {
        this.mLyricsList = sortListAlpha(lyrics);
        this.mSelectedLyricsIds = new SparseBooleanArray();
        if (!lyrics.isEmpty()) {
            Collections.sort(this.mLyricsList, new Comparator<LyricShort>() {
                @Override
                public int compare(LyricShort l1, LyricShort l2) {
                    return l1.getmLyricTitle().compareTo(l2.getmLyricTitle());
                }
            });
        }
    }

    @Override
    public int getCount() {
        return this.mLyricsList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mLyricsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LyricShortHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_lyrics_short, parent, false);
            holder = new LyricsShortAdapter.LyricShortHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (LyricsShortAdapter.LyricShortHolder) convertView.getTag();
        }

        LyricShort result = this.mLyricsList.get(position);

        holder.tvLyricsShortListTitle.setText(result.getmLyricTitle());

        Picasso.with(parent.getContext())
                .load(R.drawable.lyrics_list_icon)
                .fit()
                .centerCrop()
                .into(holder.ivLyricsShortListIcon);
        return convertView;
    }

    static class LyricShortHolder {
        @BindView(R.id.tvLyricsShortListTitle)
        TextView tvLyricsShortListTitle;
        @BindView(R.id.ivLyricsShortListIcon)
        ImageView ivLyricsShortListIcon;
        @BindView(R.id.rlListItemLyricsShort)
        RelativeLayout rlListItemLyricsShort;

        public LyricShortHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private List<LyricShort> sortListAlpha(List<LyricShort> lyricList) {
        if (!lyricList.isEmpty()) {
            Collections.sort(lyricList, new Comparator<LyricShort>() {
                @Override
                public int compare(LyricShort l1, LyricShort l2) {
                    return l1.getmLyricTitle().compareTo(l2.getmLyricTitle());
                }
            });
        }
        return lyricList;
    }

    public void removeLyric(LyricShort lyric) {
        this.mLyricsList.remove(lyric);
        notifyDataSetChanged();
    }

    public void setLyricList(List<LyricShort> lyricList) {
        this.mLyricsList = sortListAlpha(lyricList);
        notifyDataSetChanged();
    }

    public List<LyricShort> getLyricsList() {
        return this.mLyricsList;
    }

    public void toggleSelection(int position) {
        boolean oldValue = this.mSelectedLyricsIds.get(position);
        boolean newValue = !oldValue;
        if (newValue) {
            this.mSelectedLyricsIds.put(position, newValue);
        } else {
            this.mSelectedLyricsIds.delete(position);
        }
        notifyDataSetChanged();
    }

    public void removeSelected() {
        this.mSelectedLyricsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public int getSelectedLyricsIdsCount() {
        return this.mSelectedLyricsIds.size();
    }

    public boolean isSelected(int position) {
        toggleSelection(position);
        return this.mSelectedLyricsIds.get(position);
    }

    public SparseBooleanArray getSelectedLyricsIds() {
        return this.mSelectedLyricsIds;
    }
}
package stjepan.final_project.Artists;

import android.support.v4.content.ContextCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.final_project.Lyrics.Lyric;
import stjepan.final_project.R;

/**
 * Created by Stjepan on 13.2.2018..
 */

public class ArtistAdapter extends BaseAdapter {
    private List<Artist> mArtistsList;
    private SparseBooleanArray mSelectedArtistsIds;

    public ArtistAdapter() {
        this.mArtistsList = new ArrayList<>();
        this.mSelectedArtistsIds = new SparseBooleanArray();
    }

    public ArtistAdapter(List<Artist> artists) {
        this.mArtistsList = sortListAlpha(artists);
        this.mSelectedArtistsIds = new SparseBooleanArray();

    }

    @Override
    public int getCount() {
        return this.mArtistsList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mArtistsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ArtistHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_artists, parent, false);
            holder = new ArtistHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ArtistHolder) convertView.getTag();
        }

        Artist result = this.mArtistsList.get(position);

        holder.tvArtistsListName.setText(result.getmName());
        Picasso.with(parent.getContext())
                .load(R.drawable.artists_list_icon)
                .fit()
                .centerCrop()
                .into(holder.ivArtistsListIcon);
        return convertView;
    }

    static class ArtistHolder {
        @BindView(R.id.tvArtistsListName)
        TextView tvArtistsListName;
        @BindView(R.id.ivArtistsListIcon)
        ImageView ivArtistsListIcon;
        @BindView(R.id.rlListItemArtists)
        RelativeLayout rlListItemArtists;

        public ArtistHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private List<Artist> sortListAlpha(List<Artist> artistsList) {
        if (!artistsList.isEmpty()) {
            Collections.sort(artistsList, new Comparator<Artist>() {
                @Override
                public int compare(Artist artist, Artist t1) {
                    return artist.getmName().compareTo(t1.getmName());
                }
            });
        }
        return artistsList;
    }

    public void setArtistList(List<Artist> artistList) {
        this.mArtistsList = sortListAlpha(artistList);
    }

    public void removeArtist(Artist artistToRemove) {
        this.mArtistsList.remove(artistToRemove);
        notifyDataSetChanged();
    }

    public List<Artist> getArtistsList() {
        return this.mArtistsList;
    }

    public void toggleSelection(int position) {
        boolean oldValue = this.mSelectedArtistsIds.get(position);
        boolean newValue = !oldValue;
        if (newValue) {
            this.mSelectedArtistsIds.put(position, newValue);
        } else {
            this.mSelectedArtistsIds.delete(position);
        }
        notifyDataSetChanged();
    }

    public void removeSelectedArtists() {
        this.mSelectedArtistsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public int getSelectedArtistsIdsCount() {
        return this.mSelectedArtistsIds.size();
    }

    public boolean isSelected(int position) {
        toggleSelection(position);
        return this.mSelectedArtistsIds.get(position);
    }

    public SparseBooleanArray getSelectedArtistsIds() {
        return this.mSelectedArtistsIds;
    }
}

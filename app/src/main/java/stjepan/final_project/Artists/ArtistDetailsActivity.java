package stjepan.final_project.Artists;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import stjepan.final_project.Lyrics.LyricDetailsActivity;
import stjepan.final_project.Lyrics.LyricShort;
import stjepan.final_project.Lyrics.NewLyricActivity;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class ArtistDetailsActivity extends AppCompatActivity {

    public static final String ACTIVITY_NAME = "artistDetails";
    public static final String KEY_ARTIST_OBJECT = "artistObject";
    public static final String KEY_ARTIST_ID = "artistId";

    private DatabaseReference dbRefLyrics;
    private DatabaseReference dbRefArtists;

    private Artist artistObject;
    private Artist artistFromDB;
    private String artistIdFromLyrics;

    private LyricsShortAdapter adapter;

    private ActionMode actionModeRef;

    @BindView(R.id.tvArtistDetailsName)
    TextView tvArtistDetailsName;
    @BindView(R.id.lvLyricsArtistDetails)
    ListView lvLyricsArtistDetails;
    @BindView(R.id.btnAddNewLyricToArtist)
    Button btnAddNewLyricToArtist;
    @BindView(R.id.ivArtistEditIcon)
    ImageView ivArtistEditIcon;
    @BindView(R.id.btnArtistDetailsBack)
    Button btnArtistDetailsBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_details);
        ButterKnife.bind(this);

        this.dbRefArtists = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_artists));
        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));

        this.setUpListView();
        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }

    private void setUpListView() {
        this.lvLyricsArtistDetails.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        this.lvLyricsArtistDetails.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
                if (adapter.isSelected(position)) {
                    lvLyricsArtistDetails.getChildAt(position).setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.colorGray));
                } else
                    lvLyricsArtistDetails.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                actionMode.setTitle(String.valueOf(adapter.getSelectedLyricsIdsCount()) + " " + getString(R.string.cab_title_selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.menu_cab, menu);
                actionModeRef = actionMode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.itemCabSelectAll:
                        adapter.removeSelected();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            lvLyricsArtistDetails.getChildAt(i).setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.colorGray));
                            adapter.toggleSelection(i);
                        }
                        actionMode.setTitle(adapter.getCount() + " " + getString(R.string.cab_title_selected));
                        break;
                    case R.id.itemCabDelete:
                        deleteLyricConfirmation();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                adapter.removeSelected();
                for (int i = 0; i < adapter.getCount(); i++) {
                    lvLyricsArtistDetails.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                actionModeRef = null;
            }
        });
    }

    private void handleExtraData(Intent startingIntent) {
        if (startingIntent.hasExtra(this.KEY_ARTIST_OBJECT)) {
            this.artistObject = (Artist) startingIntent.getSerializableExtra(this.KEY_ARTIST_OBJECT);
            this.initializeUI(this.artistObject.getmId());
        }
        if (startingIntent.hasExtra(this.KEY_ARTIST_ID)) {
            this.artistIdFromLyrics = startingIntent.getStringExtra(this.KEY_ARTIST_ID);
            this.initializeUI(this.artistIdFromLyrics);
        }
    }

    private void initializeUI(String artistId) {
        this.dbRefArtists.child(artistId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<LyricShort> listLyricsShort = new ArrayList<>();
                Artist artist = dataSnapshot.getValue(Artist.class);

                if (artist.getmLyrics() != null && !artist.getmLyrics().isEmpty()) {
                    for (LyricShort lyric : artist.getmLyrics()) {
                        listLyricsShort.add(lyric);
                    }
                }
                artist.setmLyrics(listLyricsShort);
                tvArtistDetailsName.setText(artist.getmName());
                adapter = new LyricsShortAdapter(artist.getmLyrics());
                artistObject = artist;
                lvLyricsArtistDetails.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @OnClick(R.id.ivArtistEditIcon)
    public void editArtist() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title_editArtistName));

        final EditText input = new EditText(this);
        input.setHint(artistObject.getmName());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(getString(R.string.dialog_btn_update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newArtistName = input.getText().toString().trim();
                if (newArtistName != null && !newArtistName.isEmpty()) {
                    if (Utils.capitalize(newArtistName) != artistObject.getmName()) {
                        updateArtist(Utils.capitalize(newArtistName));
                    }
                } else {
                    Toast.makeText(ArtistDetailsActivity.this, getString(R.string.toast_error_artist_name_is_empty), Toast.LENGTH_LONG).show();
                }

            }
        });
        builder.setNegativeButton(getString(R.string.dialog_btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void updateArtist(final String newArtistName) {
        //provjeri postoji li ime u bazi
        this.dbRefArtists.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Artist artist = child.getValue(Artist.class);
                    if (newArtistName.equals(artist.getmName())) {
                        artistFromDB = new Artist();
                        artistFromDB.setmId(artist.getmId());
                        artistFromDB.setmName(artist.getmName());
                        artistFromDB.setmLyrics(artist.getmLyrics());
                    }
                }
                if (artistFromDB == null) {
                    Artist newArtist = new Artist();
                    newArtist.setmId(dbRefArtists.push().getKey());
                    newArtist.setmName(newArtistName);
                    newArtist.setmLyrics(artistObject.getmLyrics());
                    dbRefArtists.child(newArtist.getmId()).setValue(newArtist);

                    updateLyricsWithNewArtist(newArtist);
                }
                if (artistFromDB != null) {
                    if (artistFromDB.getmLyrics() == null) {
                        artistFromDB.setmLyrics(new ArrayList<LyricShort>());
                    }
                    if (artistObject.getmLyrics() != null && !artistObject.getmLyrics().isEmpty()) {
                        for (LyricShort lyricShort : artistObject.getmLyrics()) {
                            artistFromDB.getmLyrics().add(lyricShort);
                        }
                    }
                    dbRefArtists.child(artistFromDB.getmId()).setValue(artistFromDB);
                    updateLyricsWithNewArtist(artistFromDB);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateLyricsWithNewArtist(Artist ar) {
        if (ar.getmLyrics() != null && !ar.getmLyrics().isEmpty()) {
            for (LyricShort lyricShort : artistObject.getmLyrics()) {
                this.dbRefLyrics.child(lyricShort.getmLyricId()).child(getString(R.string.firebase_mArtistId)).setValue(ar.getmId());
                this.dbRefLyrics.child(lyricShort.getmLyricId()).child(getString(R.string.firebase_mArtistName)).setValue(ar.getmName());
            }
        }
        //obrisi "starog"
        this.dbRefArtists.child(artistObject.getmId()).removeValue();
        goToArtistDetails(ar);
    }

    private void goToArtistDetails(Artist artist) {
        Intent intent = new Intent(this, ArtistDetailsActivity.class);
        intent.putExtra(ArtistDetailsActivity.KEY_ARTIST_OBJECT, artist);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btnArtistDetailsBack)
    public void goToFragment() {
        Utils.goToFragment(this, ArtistsFragment.TITLE);
        finish();
    }

    @OnClick(R.id.btnAddNewLyricToArtist)
    public void addNewLyricToArtist() {
        Intent intent = new Intent(this, NewLyricActivity.class);
        intent.putExtra(NewLyricActivity.KEY_ARTIST_NAME, this.tvArtistDetailsName.getText().toString());
        intent.putExtra(NewLyricActivity.KEY_CALLED_FROM, this.ACTIVITY_NAME);
        intent.putExtra(NewLyricActivity.KEY_ARTIST_ID, this.artistObject.getmId());
        this.startActivity(intent);
        finish();
    }

    @OnItemClick(R.id.lvLyricsArtistDetails)
    public void showLyricDetails(int position) {
        LyricShort lyricShort = (LyricShort) lvLyricsArtistDetails.getAdapter().getItem(position);
        Intent intent = new Intent(this, LyricDetailsActivity.class);
        intent.putExtra(LyricDetailsActivity.KEY_LYRIC_SHORT_OBJECT, lyricShort);
        intent.putExtra(LyricDetailsActivity.KEY_CALLING_ARTIST_ID, this.artistObject.getmId());
        this.startActivity(intent);
        finish();
    }

    @OnItemLongClick(R.id.lvLyricsArtistDetails)
    public boolean deleteLyricConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title_delete));
        builder.setMessage(getString(R.string.dialog_msg_delete_lyric_confirm));
        builder.setPositiveButton(getString(R.string.dialog_btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteLyric();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
        return true;
    }

    private void deleteLyric() {
        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));
        this.dbRefArtists = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_artists)).child(artistObject.getmId());
        SparseBooleanArray selectedLyrics = adapter.getSelectedLyricsIds();

        for (int i = (selectedLyrics.size() - 1); i >= 0; i--) {
            if (selectedLyrics.valueAt(i)) {
                LyricShort lyricShort = (LyricShort) adapter.getItem(selectedLyrics.keyAt(i));
                adapter.removeLyric(lyricShort);
                this.dbRefLyrics.child(lyricShort.getmLyricId()).removeValue();
                this.artistObject.getmLyrics().remove(lyricShort);
                this.dbRefArtists.setValue(artistObject);
            }
        }
        this.dismissCAB();
    }

    private void dismissCAB() {
        if (this.actionModeRef != null) {
            this.actionModeRef.finish();
        }
    }
}

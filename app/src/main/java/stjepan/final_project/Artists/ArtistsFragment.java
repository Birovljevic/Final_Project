package stjepan.final_project.Artists;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import stjepan.final_project.Lyrics.LyricShort;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class ArtistsFragment extends Fragment implements ValueEventListener {

    public static final String TITLE = "Artists";

    private DatabaseReference dbRefArtists;
    private DatabaseReference dbRefLyrics;
    private ArtistAdapter adapter;

    private ActionMode actionModeRef;
    private android.support.v7.widget.Toolbar toolbar;

    @BindView(R.id.lvArtistsFragment)
    ListView lvArtistsFragment;
    @BindView(R.id.btnAddNewArtist)
    Button btnAddNewArtist;

    public ArtistsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artists, container, false);
        ButterKnife.bind(this, view);
        this.toolbar = getActivity().findViewById(R.id.tMainToolbar);
        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));
        this.dbRefArtists = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_artists));
        this.dbRefArtists.addValueEventListener(this);
        this.setUpListView();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissCAB();
    }

    private void dismissCAB() {
        if (this.actionModeRef != null) {
            this.actionModeRef.finish();
        }
        this.toolbar.setVisibility(View.VISIBLE);
    }

    private void setUpListView() {
        this.lvArtistsFragment.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        this.lvArtistsFragment.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
                if (adapter.isSelected(position)) {
                    lvArtistsFragment.getChildAt(position).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorGray));
                } else
                    lvArtistsFragment.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                actionMode.setTitle(adapter.getSelectedArtistsIdsCount() + " " + getString(R.string.cab_title_selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.menu_cab, menu);
                actionModeRef = actionMode;
                toolbar.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.itemCabSelectAll:
                        adapter.removeSelectedArtists();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            lvArtistsFragment.getChildAt(i).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorGray));
                            adapter.toggleSelection(i);
                        }
                        actionMode.setTitle(adapter.getCount() + " " + getString(R.string.cab_title_selected));
                        break;
                    case R.id.itemCabDelete:
                        deleteArtistConfirmation();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                adapter.removeSelectedArtists();
                for (int i = 0; i < adapter.getCount(); i++) {
                    lvArtistsFragment.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                actionModeRef = null;
                toolbar.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnClick(R.id.btnAddNewArtist)
    public void addNewArtist() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.dialog_title_newArtistName));
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(getString(R.string.dialog_btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newArtistName = input.getText().toString().trim();
                if (newArtistName != null && !newArtistName.isEmpty()) {
                    Artist artist = new Artist();
                    artist.setmName(Utils.capitalize(newArtistName));
                    artist.setmId(dbRefArtists.push().getKey());
                    dbRefArtists.child(artist.getmId()).setValue(artist);
                    Toast.makeText(getContext(), getString(R.string.toast_artist_successfully_saved), Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(getContext(), getString(R.string.toast_error_artist_name_is_empty), Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        List<Artist> artistList = new ArrayList<>();
        for (DataSnapshot child : dataSnapshot.getChildren()) {
            Artist artist = child.getValue(Artist.class);
            artistList.add(artist);
        }
        this.adapter = new ArtistAdapter();
        adapter.setArtistList(artistList);
        this.lvArtistsFragment.setAdapter(adapter);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
    }

    @OnItemClick(R.id.lvArtistsFragment)
    public void artistDetails(int position, View view) {
        Artist artist = (Artist) lvArtistsFragment.getAdapter().getItem(position);
        Intent intent = new Intent(getContext(), ArtistDetailsActivity.class);
        intent.putExtra(ArtistDetailsActivity.KEY_ARTIST_OBJECT, artist);
        this.startActivity(intent);
    }

    @OnItemLongClick(R.id.lvArtistsFragment)
    public boolean deleteArtistConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.dialog_title_delete);
        builder.setMessage(R.string.dialog_msg_delete_artist_confirm);
        builder.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteArtists();
            }
        });
        builder.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
        return true;
    }

    private void deleteArtists() {
        SparseBooleanArray selectedArtists = adapter.getSelectedArtistsIds();
        for (int i = (selectedArtists.size() - 1); i >= 0; i--) {
            if (selectedArtists.valueAt(i)) {
                Artist artistToRemove = (Artist) adapter.getItem(selectedArtists.keyAt(i));
                adapter.removeArtist(artistToRemove);
                if (artistToRemove.getmLyrics() != null && !artistToRemove.getmLyrics().isEmpty()) {
                    for (LyricShort lyric : artistToRemove.getmLyrics()) {
                        this.dbRefLyrics.child(lyric.getmLyricId()).removeValue();
                    }
                }
                this.dbRefArtists.child(artistToRemove.getmId()).removeValue();
                Toast.makeText(getContext(), R.string.toast_artists_successfully_deleted, Toast.LENGTH_SHORT).show();
            }
        }
        dismissCAB();
    }
}

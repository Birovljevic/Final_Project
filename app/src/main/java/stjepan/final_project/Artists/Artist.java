package stjepan.final_project.Artists;

import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

import stjepan.final_project.Lyrics.LyricShort;

/**
 * Created by Stjepan on 13.2.2018..
 */

public class Artist implements Serializable {

    private String mId;
    private String mName;
    private List<LyricShort> mLyrics;

    public Artist() {
    }

    public Artist(String mId, String mName, List<LyricShort> mLyrics) {
        this.mId = mId;
        this.mName = mName;
        this.mLyrics = mLyrics;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public List<LyricShort> getmLyrics() {
        return mLyrics;
    }

    public void setmLyrics(List<LyricShort> mLyrics) {
        this.mLyrics = mLyrics;
    }
}

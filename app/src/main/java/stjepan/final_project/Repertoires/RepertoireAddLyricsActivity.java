package stjepan.final_project.Repertoires;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import stjepan.final_project.Lyrics.Lyric;
import stjepan.final_project.Lyrics.LyricsAdapter;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class RepertoireAddLyricsActivity extends Activity implements ValueEventListener{

    public static final String KEY_REPERTOIRE_OBJECT = "repertoireObjectAddLyrics";

    private DatabaseReference dbRefRepertoires;
    private DatabaseReference dbRefLyrics;

    private Repertoire repertoireObject;

    private LyricsAdapter adapter;
    private ActionMode actionModeRef;

    @BindView(R.id.lvRepertoireAddLyrics)
    ListView lvRepertoireAddLyrics;
    @BindView(R.id.btnRepertoireAddLyricsBack)
    Button btnRepertoireAddLyricsBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repertoire_add_lyrics);
        ButterKnife.bind(this);

        this.dbRefRepertoires = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_repertoires));
        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));
        this.dbRefLyrics.addValueEventListener(this);

        this.setUpListView();
        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }

    private void setUpListView() {
        this.lvRepertoireAddLyrics.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        this.lvRepertoireAddLyrics.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
                if (adapter.isSelected(position)){
                    lvRepertoireAddLyrics.getChildAt(position).setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.colorGray));
                }else
                    lvRepertoireAddLyrics.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                actionMode.setTitle(String.valueOf(adapter.getSelectedLyricsIdsCount()) + " " + getString(R.string.cab_title_selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.menu_cab_add_lyrics, menu);
                actionModeRef = actionMode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch(menuItem.getItemId()){
                    case R.id.itemCabSelectAllLyrics:
                        adapter.removeSelected();
                        for (int i = 0; i < adapter.getCount(); i++){
                            lvRepertoireAddLyrics.getChildAt(i).setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.colorGray));
                            adapter.toggleSelection(i);
                        }
                        actionMode.setTitle(adapter.getCount() + " " +  getString(R.string.cab_title_selected));
                        break;
                    case R.id.itemCabAddLyrics:
                        addLyricsConfirmation();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                adapter.removeSelected();
                for (int i = 0; i < adapter.getCount(); i++){
                    lvRepertoireAddLyrics.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                actionModeRef = null;
            }
        });
    }
    private void handleExtraData(Intent startingIntent) {
        if (startingIntent.hasExtra(this.KEY_REPERTOIRE_OBJECT)) {
            this.repertoireObject = (Repertoire) startingIntent.getSerializableExtra(this.KEY_REPERTOIRE_OBJECT);
        }
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        List<Lyric> lyricList = new ArrayList<>();
        for (DataSnapshot child : dataSnapshot.getChildren()) {
            Lyric lyric = child.getValue(Lyric.class);
            lyricList.add(lyric);
        }
        adapter = new LyricsAdapter(lyricList);
        this.lvRepertoireAddLyrics.setAdapter(adapter);
    }
    @Override
    public void onCancelled(DatabaseError databaseError) {
    }

    private void addLyricsConfirmation(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title_repertoireAddLyrics));
        builder.setMessage(getString(R.string.dialog_msg_repertoireAddLyrics));
        builder.setPositiveButton(getString(R.string.dialog_btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                addLyrics();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    private void addLyrics(){
        this.dbRefRepertoires = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_repertoires)).child(repertoireObject.getmId());
        SparseBooleanArray selectedLyrics = adapter.getSelectedLyricsIds();

        List<Lyric> list = this.repertoireObject.getmLyricsList();

        for (int i = (selectedLyrics.size() - 1); i >= 0; i--){
            if (selectedLyrics.valueAt(i)){
                Lyric lyric = (Lyric) adapter.getItem(selectedLyrics.keyAt(i));
                list.add(lyric);
            }
        }
        this.repertoireObject.setmLyricsList(list);
        this.dbRefRepertoires.setValue(repertoireObject);
        this.dismissCAB();
        Toast.makeText(this, getString(R.string.toast_repertoire_successfully_added_lyrics), Toast.LENGTH_SHORT).show();
        goToDetails();

    }
    private void dismissCAB(){
        if (this.actionModeRef != null){
            this.actionModeRef.finish();
        }
    }
    private void goToDetails(){
        Intent intent = new Intent(this, RepertoireDetailsActivity.class);
        intent.putExtra(RepertoireDetailsActivity.KEY_REPERTOIRE_OBJECT, this.repertoireObject);
        startActivity(intent);
        finish();
    }
    @OnClick(R.id.btnRepertoireAddLyricsBack)
    public void goToRepertoiresFragment(){
        Utils.goToFragment(this, RepertoiresFragment.TITLE);
        finish();
    }
}
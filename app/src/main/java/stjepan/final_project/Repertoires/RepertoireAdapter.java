package stjepan.final_project.Repertoires;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.R;

/**
 * Created by Stjepan on 13.2.2018..
 */

public class RepertoireAdapter extends BaseAdapter {
    private List<Repertoire> mRepertoiresList;

    public RepertoireAdapter() {
        this.mRepertoiresList = new ArrayList<>();
    }

    public RepertoireAdapter(List<Repertoire> repertoires) {
        this.mRepertoiresList = sortListAlpha(repertoires);
    }

    @Override
    public int getCount() {
        return this.mRepertoiresList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mRepertoiresList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RepertoireHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_repertoires, parent, false);
            holder = new RepertoireHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (RepertoireHolder) convertView.getTag();
        }

        Repertoire result = this.mRepertoiresList.get(position);

        holder.tvRepertoiresListName.setText(result.getmTitle());
        Picasso.with(parent.getContext())
                .load(R.drawable.repertoires_icon)
                .fit()
                .centerCrop()
                .into(holder.ivRepertoiresListIcon);
        return convertView;
    }

    static class RepertoireHolder {
        @BindView(R.id.tvRepertoiresListName)
        TextView tvRepertoiresListName;
        @BindView(R.id.ivRepertoiresListIcon)
        ImageView ivRepertoiresListIcon;

        public RepertoireHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private List<Repertoire> sortListAlpha(List<Repertoire> repertoiresList) {
        if (!repertoiresList.isEmpty()) {
            Collections.sort(repertoiresList, new Comparator<Repertoire>() {
                @Override
                public int compare(Repertoire r1, Repertoire r2) {
                    return r1.getmTitle().compareTo(r2.getmTitle());
                }
            });
        }
        return repertoiresList;
    }

    public void setRepertoireList(List<Repertoire> repertoireList) {
        this.mRepertoiresList = sortListAlpha(repertoireList);
    }

    public void removeRepertoire(Repertoire repertoireToRemove) {
        this.mRepertoiresList.remove(repertoireToRemove);
        notifyDataSetChanged();
    }

    public List<Repertoire> getRepertoiresList() {
        return this.mRepertoiresList;
    }
}

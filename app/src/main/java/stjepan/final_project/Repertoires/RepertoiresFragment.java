package stjepan.final_project.Repertoires;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.Artists.ArtistAdapter;
import stjepan.final_project.Artists.ArtistDetailsActivity;
import stjepan.final_project.Lyrics.LyricShort;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class RepertoiresFragment extends Fragment implements ValueEventListener {

    public static final String TITLE = "Repos";

    private DatabaseReference dbRefRepertoires;
    private RepertoireAdapter adapter;
    private List<Repertoire> repertoires;

    @BindView(R.id.lvRepertoiresFragment)
    ListView lvRepertoiresFragment;
    @BindView(R.id.btnAddNewRepertoire)
    Button btnAddNewRepertoire;

    public RepertoiresFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repertoires, container, false);
        ButterKnife.bind(this, view);

        this.dbRefRepertoires = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_repertoires));
        this.dbRefRepertoires.addValueEventListener(this);
        return view;
    }

    @OnClick(R.id.btnAddNewRepertoire)
    public void addNewRepertoire() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.dialog_title_newRepertoireTitle));

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(getString(R.string.dialog_btn_save), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newRepertoireTitle = input.getText().toString().trim();
                if (newRepertoireTitle != null && !newRepertoireTitle.isEmpty()) {
                    Repertoire repertoire = new Repertoire();
                    repertoire.setmTitle(Utils.capitalize(newRepertoireTitle));
                    repertoire.setmId(dbRefRepertoires.push().getKey());
                    dbRefRepertoires.child(repertoire.getmId()).setValue(repertoire);
                    Toast.makeText(getContext(), getString(R.string.toast_repertoire_successfully_saved), Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(getContext(), getString(R.string.toast_error_repertoire_title_is_empty), Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        List<Repertoire> repertoireList = new ArrayList<>();
        for (DataSnapshot child : dataSnapshot.getChildren()) {
            Repertoire repertoire = child.getValue(Repertoire.class);
            repertoireList.add(repertoire);
        }
        this.adapter = new RepertoireAdapter();
        adapter.setRepertoireList(repertoireList);
        this.lvRepertoiresFragment.setAdapter(adapter);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
    }

    @OnItemClick(R.id.lvRepertoiresFragment)
    public void repertoireDetails(int position, View view) {
        Repertoire repertoire = (Repertoire) lvRepertoiresFragment.getAdapter().getItem(position);
        Intent intent = new Intent(getContext(), RepertoireDetailsActivity.class);
        intent.putExtra(RepertoireDetailsActivity.KEY_REPERTOIRE_OBJECT, repertoire);
        this.startActivity(intent);
    }

    @OnItemLongClick(R.id.lvRepertoiresFragment)
    public boolean deleteRepertoireConfirmation(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.dialog_title_delete);
        builder.setMessage(R.string.dialog_msg_delete_repertoire_confirm);
        builder.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteRepertoire(position);
            }
        });
        builder.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
        return true;
    }

    private void deleteRepertoire(int position) {
        Repertoire repertoireToRemove = (Repertoire) adapter.getItem(position);
        adapter.removeRepertoire(repertoireToRemove);
        this.dbRefRepertoires.child(repertoireToRemove.getmId()).removeValue();
        Toast.makeText(getContext(), getString(R.string.toast_repertoire_successfully_deleted), Toast.LENGTH_SHORT).show();
    }
}

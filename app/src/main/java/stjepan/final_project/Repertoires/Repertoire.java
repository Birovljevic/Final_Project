package stjepan.final_project.Repertoires;

import java.io.Serializable;
import java.util.List;

import stjepan.final_project.Lyrics.Lyric;

/**
 * Created by Stjepan on 10.3.2018..
 */

public class Repertoire implements Serializable {
    private String mId;
    private String mTitle;
    private List<Lyric> mLyricsList;

    public Repertoire() {
    }

    public Repertoire(String mId, String mTitle, List<Lyric> mLyricsList) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mLyricsList = mLyricsList;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public List<Lyric> getmLyricsList() {
        return mLyricsList;
    }

    public void setmLyricsList(List<Lyric> mLyricsList) {
        this.mLyricsList = mLyricsList;
    }
}

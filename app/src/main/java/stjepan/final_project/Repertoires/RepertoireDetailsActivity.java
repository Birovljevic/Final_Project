package stjepan.final_project.Repertoires;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import stjepan.final_project.Artists.Artist;
import stjepan.final_project.Artists.ArtistDetailsActivity;
import stjepan.final_project.Artists.ArtistsFragment;
import stjepan.final_project.Artists.LyricsShortAdapter;
import stjepan.final_project.Lyrics.Lyric;
import stjepan.final_project.Lyrics.LyricDetailsActivity;
import stjepan.final_project.Lyrics.LyricShort;
import stjepan.final_project.Lyrics.LyricsAdapter;
import stjepan.final_project.Lyrics.NewLyricActivity;
import stjepan.final_project.R;
import stjepan.final_project.Utils;

public class RepertoireDetailsActivity extends Activity {

    public static final String KEY_REPERTOIRE_OBJECT = "repertoireObjectDetails";

    private Repertoire repertoireObject;
    private Repertoire repertoireFromDB;

    private DatabaseReference dbRefRepertoires;
    private DatabaseReference dbRefLyrics;

    private LyricsAdapter adapter;
    private ActionMode actionModeRef;

    @BindView(R.id.tvRepertoireDetailsName)
    TextView tvRepertoireDetailsName;
    @BindView(R.id.lvLyricsRepertoireDetails)
    ListView lvLyricsRepertoireDetails;
    @BindView(R.id.btnAddNewLyricsToRepertoire)
    Button btnAddNewLyricsToRepertoire;
    @BindView(R.id.ivRepertoireEditIcon)
    ImageView ivRepertoireEditIcon;
    @BindView(R.id.btnRepertoireDetailsBack)
    Button btnRepertoireDetailsBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repertoire_details);
        ButterKnife.bind(this);

        this.dbRefRepertoires = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_repertoires));
        this.dbRefLyrics = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_lyrics));

        this.setUpListView();
        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }

    private void setUpListView() {
        this.lvLyricsRepertoireDetails.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        this.lvLyricsRepertoireDetails.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
                if (adapter.isSelected(position)) {
                    lvLyricsRepertoireDetails.getChildAt(position).setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.colorGray));
                } else
                    lvLyricsRepertoireDetails.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                actionMode.setTitle(String.valueOf(adapter.getSelectedLyricsIdsCount()) + " " + getString(R.string.cab_title_selected));
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.menu_cab, menu);
                actionModeRef = actionMode;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.itemCabSelectAll:
                        adapter.removeSelected();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            lvLyricsRepertoireDetails.getChildAt(i).setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.colorGray));
                            adapter.toggleSelection(i);
                        }
                        actionMode.setTitle(adapter.getCount() + " " + getString(R.string.cab_title_selected));
                        break;
                    case R.id.itemCabDelete:
                        deleteLyricConfirmation();
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                adapter.removeSelected();
                for (int i = 0; i < adapter.getCount(); i++) {
                    lvLyricsRepertoireDetails.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                actionModeRef = null;
            }
        });
    }

    private void handleExtraData(Intent startingIntent) {
        if (startingIntent.hasExtra(this.KEY_REPERTOIRE_OBJECT)) {
            this.repertoireObject = (Repertoire) startingIntent.getSerializableExtra(this.KEY_REPERTOIRE_OBJECT);
            this.initializeUI(this.repertoireObject.getmId());
        }
    }

    private void initializeUI(String artistId) {
        this.dbRefRepertoires.child(artistId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Lyric> listLyrics = new ArrayList<>();
                Repertoire repertoire = dataSnapshot.getValue(Repertoire.class);

                if (repertoire.getmLyricsList() != null && !repertoire.getmLyricsList().isEmpty()) {
                    for (Lyric lyric : repertoire.getmLyricsList()) {
                        listLyrics.add(lyric);
                    }
                }
                repertoire.setmLyricsList(listLyrics);
                tvRepertoireDetailsName.setText(repertoire.getmTitle());
                adapter = new LyricsAdapter(repertoire.getmLyricsList());
                repertoireObject = repertoire;
                lvLyricsRepertoireDetails.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @OnClick(R.id.ivRepertoireEditIcon)
    public void editRepertoire() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title_editRepertoireTitle));
        final EditText input = new EditText(this);
        input.setHint(repertoireObject.getmTitle());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(getString(R.string.dialog_btn_update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newRepertoireTitle = input.getText().toString().trim();
                if (newRepertoireTitle != null && !newRepertoireTitle.isEmpty()) {
                    if (Utils.capitalize(newRepertoireTitle) != repertoireObject.getmTitle()) {
                        updateRepertoire(Utils.capitalize(newRepertoireTitle));
                    }
                } else {
                    Toast.makeText(RepertoireDetailsActivity.this, getString(R.string.toast_error_repertoire_title_is_empty), Toast.LENGTH_LONG).show();
                }

            }
        });
        builder.setNegativeButton(getString(R.string.dialog_btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void updateRepertoire(final String newRepertoireTitle) {
        //provjera postoji li u bazi
        this.dbRefRepertoires.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Repertoire repertoire = child.getValue(Repertoire.class);
                    if (newRepertoireTitle.equals(repertoire.getmTitle())) {
                        repertoireFromDB = new Repertoire();
                        repertoireFromDB.setmId(repertoire.getmId());
                        repertoireFromDB.setmTitle(repertoire.getmTitle());
                        repertoireFromDB.setmLyricsList(repertoire.getmLyricsList());
                    }
                }
                if (repertoireFromDB == null) {
                    Repertoire newRepertoire = new Repertoire();
                    newRepertoire.setmId(dbRefRepertoires.push().getKey());
                    newRepertoire.setmTitle(newRepertoireTitle);
                    newRepertoire.setmLyricsList(repertoireObject.getmLyricsList());
                    dbRefRepertoires.child(newRepertoire.getmId()).setValue(newRepertoire);
                }
                if (repertoireFromDB != null) {
                    if (repertoireFromDB.getmLyricsList() == null) {
                        repertoireFromDB.setmLyricsList(new ArrayList<Lyric>());
                    }
                    if (repertoireObject.getmLyricsList() != null && !repertoireObject.getmLyricsList().isEmpty()) {
                        for (Lyric lyric : repertoireObject.getmLyricsList()) {
                            repertoireFromDB.getmLyricsList().add(lyric);
                        }
                    }
                    dbRefRepertoires.child(repertoireFromDB.getmId()).setValue(repertoireFromDB);
                }
                //obrisi starog
                dbRefRepertoires.child(repertoireObject.getmId()).removeValue();
                Toast.makeText(RepertoireDetailsActivity.this, getString(R.string.toast_repertoire_successfully_updated), Toast.LENGTH_SHORT).show();
                goToFragment();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @OnClick(R.id.btnRepertoireDetailsBack)
    public void goToFragment() {
        Utils.goToFragment(this, RepertoiresFragment.TITLE);
        finish();
    }
    @Override
    public void onBackPressed(){
        goToFragment();
    }

    @OnClick(R.id.btnAddNewLyricsToRepertoire)
    public void addNewLyricsToRepertoire() {
        Intent intent = new Intent(this, RepertoireAddLyricsActivity.class);
        intent.putExtra(RepertoireAddLyricsActivity.KEY_REPERTOIRE_OBJECT, this.repertoireObject);
        this.startActivity(intent);
        finish();
    }

    @OnItemClick(R.id.lvLyricsRepertoireDetails)
    public void showLyricDetails(int position) {
        Lyric lyric = (Lyric) lvLyricsRepertoireDetails.getAdapter().getItem(position);
        Intent intent = new Intent(this, LyricDetailsActivity.class);
        intent.putExtra(LyricDetailsActivity.KEY_LYRIC_OBJECT, lyric);
        intent.putExtra(LyricDetailsActivity.KEY_CALLING_REPERTOIRE_OBJECT, this.repertoireObject);
        this.startActivity(intent);
        finish();
    }

    public boolean deleteLyricConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_title_delete));
        builder.setMessage(getString(R.string.dialog_msg_repertoireDeleteLyrics_confirm));
        builder.setPositiveButton(getString(R.string.dialog_btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteLyricFromRepertoire();
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
        return true;
    }

    private void deleteLyricFromRepertoire() {
        this.dbRefRepertoires = FirebaseDatabase.getInstance().getReference(getString(R.string.firebase_repertoires)).child(repertoireObject.getmId());
        SparseBooleanArray selectedLyrics = adapter.getSelectedLyricsIds();

        for (int i = (selectedLyrics.size() - 1); i >= 0; i--) {
            if (selectedLyrics.valueAt(i)) {
                Lyric lyric = (Lyric) adapter.getItem(selectedLyrics.keyAt(i));
                adapter.removeLyric(lyric);
            }
        }
        this.repertoireObject.setmLyricsList(adapter.getLyricsList());
        this.dbRefRepertoires.setValue(repertoireObject);
        this.dismissCAB();

    }

    private void dismissCAB() {
        if (this.actionModeRef != null) {
            this.actionModeRef.finish();
        }
    }
}

